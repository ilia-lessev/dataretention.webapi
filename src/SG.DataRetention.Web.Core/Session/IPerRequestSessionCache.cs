﻿using System.Threading.Tasks;
using SG.DataRetention.Sessions.Dto;

namespace SG.DataRetention.Web.Session
{
    public interface IPerRequestSessionCache
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformationsAsync();
    }
}
