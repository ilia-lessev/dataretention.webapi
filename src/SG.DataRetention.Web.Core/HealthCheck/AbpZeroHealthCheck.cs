﻿using Microsoft.Extensions.DependencyInjection;
using SG.DataRetention.HealthChecks;

namespace SG.DataRetention.Web.HealthCheck
{
    public static class AbpZeroHealthCheck
    {
        public static IHealthChecksBuilder AddAbpZeroHealthCheck(this IServiceCollection services)
        {
            var builder = services.AddHealthChecks();
            builder.AddCheck<DataRetentionDbContextHealthCheck>("Database Connection");
            builder.AddCheck<DataRetentionDbContextUsersHealthCheck>("Database Connection with user check");
            builder.AddCheck<CacheHealthCheck>("Cache");

            // add your custom health checks here
            // builder.AddCheck<MyCustomHealthCheck>("my health check");

            return builder;
        }
    }
}
