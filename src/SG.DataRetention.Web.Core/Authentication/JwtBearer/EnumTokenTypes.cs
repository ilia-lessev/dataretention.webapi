﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SG.DataRetention.Web.Authentication.JwtBearer
{
    public enum TokenType
    {
        AccessToken,
        RefreshToken
    }
}
