﻿using Microsoft.AspNetCore.Mvc;
using SG.DataRetention.Web.Controllers;

namespace SG.DataRetention.Web.Public.Controllers
{
    public class AboutController : DataRetentionControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}