﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace SG.DataRetention.Web.Public.Views
{
    public abstract class DataRetentionViewComponent : AbpViewComponent
    {
        protected DataRetentionViewComponent()
        {
            LocalizationSourceName = DataRetentionConsts.LocalizationSourceName;
        }
    }
}