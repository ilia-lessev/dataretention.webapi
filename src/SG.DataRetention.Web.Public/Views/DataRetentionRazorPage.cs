﻿using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace SG.DataRetention.Web.Public.Views
{
    public abstract class DataRetentionRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected DataRetentionRazorPage()
        {
            LocalizationSourceName = DataRetentionConsts.LocalizationSourceName;
        }
    }
}
