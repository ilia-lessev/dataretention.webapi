﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace SG.DataRetention
{
    [DependsOn(typeof(DataRetentionXamarinSharedModule))]
    public class DataRetentionXamarinAndroidModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DataRetentionXamarinAndroidModule).GetAssembly());
        }
    }
}