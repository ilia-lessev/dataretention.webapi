﻿using Abp.IdentityServer4;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using SG.DataRetention.Authorization.Roles;
using SG.DataRetention.Authorization.Users;
using SG.DataRetention.Chat;
using SG.DataRetention.Editions;
using SG.DataRetention.Friendships;
using SG.DataRetention.MultiTenancy;
using SG.DataRetention.MultiTenancy.Accounting;
using SG.DataRetention.MultiTenancy.Payments;
using SG.DataRetention.Storage;
using SG.DataRetention.Password;
using SG.DataRetention.Queries;

namespace SG.DataRetention.EntityFrameworkCore
{
    public class DataRetentionDbContext : AbpZeroDbContext<Tenant, Role, User, DataRetentionDbContext>, IAbpPersistedGrantDbContext
    {
        /* Define an IDbSet for each entity of the application */

        public virtual DbSet<BinaryObject> BinaryObjects { get; set; }

        public virtual DbSet<Friendship> Friendships { get; set; }

        public virtual DbSet<ChatMessage> ChatMessages { get; set; }

        public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }

        public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }

        public virtual DbSet<Invoice> Invoices { get; set; }

        public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }

        public virtual DbSet<PasswordHistory> PasswordHistories { get; set; }           

        public virtual DbSet<Department> Departments { get; set; }

        public virtual DbSet<Query> Queries { get; set; }

        public virtual DbSet<Attachments> Attachments { get; set; }

        public virtual DbSet<QueryType> QueryTypes { get; set; }

        public virtual DbSet<DepartmentQueryType> DepartmentQueryTypes { get; set; }

        public DataRetentionDbContext(DbContextOptions<DataRetentionDbContext> options)
            : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<BinaryObject>(b =>
            {
                b.HasIndex(e => new { e.TenantId });
            });

            modelBuilder.Entity<ChatMessage>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
            });

            modelBuilder.Entity<Friendship>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId });
                b.HasIndex(e => new { e.TenantId, e.FriendUserId });
                b.HasIndex(e => new { e.FriendTenantId, e.UserId });
                b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
            });

            modelBuilder.Entity<Tenant>(b =>
            {
                b.HasIndex(e => new { e.SubscriptionEndDateUtc });
                b.HasIndex(e => new { e.CreationTime });
            });

            modelBuilder.Entity<SubscriptionPayment>(b =>
            {
                b.HasIndex(e => new { e.Status, e.CreationTime });
                b.HasIndex(e => new { PaymentId = e.ExternalPaymentId, e.Gateway });
            });
                   

            modelBuilder.ConfigurePersistedGrantEntity();
        }
    }
}
