using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace SG.DataRetention.EntityFrameworkCore
{
    public static class DataRetentionDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<DataRetentionDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<DataRetentionDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}