﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using SG.DataRetention.Configuration;
using SG.DataRetention.Web;

namespace SG.DataRetention.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class DataRetentionDbContextFactory : IDesignTimeDbContextFactory<DataRetentionDbContext>
    {
        public DataRetentionDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DataRetentionDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder(), addUserSecrets: true);

            DataRetentionDbContextConfigurer.Configure(builder, configuration.GetConnectionString(DataRetentionConsts.ConnectionStringName));

            return new DataRetentionDbContext(builder.Options);
        }
    }
}