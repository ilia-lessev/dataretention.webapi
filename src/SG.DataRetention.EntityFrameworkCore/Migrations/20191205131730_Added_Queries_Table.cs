﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SG.DataRetention.Migrations
{
    public partial class Added_Queries_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Queries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QueryId = table.Column<int>(nullable: false),
                    QueryReference = table.Column<string>(nullable: true),
                    AssignedTo = table.Column<string>(nullable: true),
                    Department = table.Column<string>(nullable: true),
                    QueryType = table.Column<string>(nullable: true),
                    QueryDescription = table.Column<string>(nullable: true),
                    QueryStatus = table.Column<string>(nullable: true),
                    SLA = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: true),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Queries", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Queries");
        }
    }
}
