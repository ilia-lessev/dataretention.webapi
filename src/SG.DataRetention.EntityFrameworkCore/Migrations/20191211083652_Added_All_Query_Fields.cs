﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SG.DataRetention.Migrations
{
    public partial class Added_All_Query_Fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AuthorisedContactPerson",
                table: "Queries",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CashAccountNumber",
                table: "Queries",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "CompanyName",
                table: "Queries",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyRegistrationNumber",
                table: "Queries",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ContactNumber",
                table: "Queries",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "DateFrom",
                table: "Queries",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DateTo",
                table: "Queries",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DepartmentName",
                table: "Queries",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Queries",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EntityNumber",
                table: "Queries",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "FinancialInstrument_Bonds",
                table: "Queries",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "FinancialInstrument_Derivatives",
                table: "Queries",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "FinancialInstrument_Equities",
                table: "Queries",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "FinancialInstrument_MoneyMarket",
                table: "Queries",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "SecurityAccountNumber",
                table: "Queries",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AuthorisedContactPerson",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "CashAccountNumber",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "CompanyName",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "CompanyRegistrationNumber",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "ContactNumber",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "DateFrom",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "DateTo",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "DepartmentName",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "EntityNumber",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "FinancialInstrument_Bonds",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "FinancialInstrument_Derivatives",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "FinancialInstrument_Equities",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "FinancialInstrument_MoneyMarket",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "SecurityAccountNumber",
                table: "Queries");
        }
    }
}
