﻿using SG.DataRetention.EntityFrameworkCore;

namespace SG.DataRetention.Migrations.Seed.Host
{
    public class InitialHostDbBuilder
    {
        private readonly DataRetentionDbContext _context;

        public InitialHostDbBuilder(DataRetentionDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
            new InitialPasswordCreator(_context).Create();         

            _context.SaveChanges();
        }
    }
}
