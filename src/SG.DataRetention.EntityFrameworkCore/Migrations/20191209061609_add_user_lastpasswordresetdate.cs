﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SG.DataRetention.Migrations
{
    public partial class add_user_lastpasswordresetdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "LastResetTimeStamp",
                table: "AbpUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastResetTimeStamp",
                table: "AbpUsers");
        }
    }
}
