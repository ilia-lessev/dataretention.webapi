﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Oracle.ManagedDataAccess.Client;


namespace SG.DataRetention.DataQueries.Common
{
    class DbParamDescriptorBase
    {
        public string ParamName { get; set; }
        public OracleDbType DbType { get; set; }
        public QueryFilterTypes FilterType { get; set; }
        public Type IncomingType { get; set; }
        public ParameterDirection ParamDirection { get; set; }
    }
}
