﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Oracle.ManagedDataAccess.Client;


namespace SG.DataRetention.DataQueries.Common
{
    public enum QueryFilterTypes
    {
        DateRangeStart, //>=
        DateRangeEnd, //<=
        Standard //=
    }
}
