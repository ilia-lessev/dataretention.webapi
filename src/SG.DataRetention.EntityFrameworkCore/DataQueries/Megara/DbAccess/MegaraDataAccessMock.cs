﻿using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using SG.DataRetention.Configuration;
using SG.DataRetention.DataQueries.Common.Dto;
using SG.DataRetention.DataQueries.Megara.Dto;

using SG.DataRetention.Web;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SG.DataRetention.DataQueries.Megara
{

    public class MegaraDataAccessMock : IMegaraDataAccess
    {
        public MegaraDataAccessMock()
        { }



        public async Task<List<ClientInstructionsEquityOutput>> GetClientInstructionsEquityAsync(ClientInstructionsEquityParams input, DataPagingInput paging)
        {
            //var oID_value = (input.GetType().GetProperty("OrderID").GetValue(input) == null ? " N U L L " : input.GetType().GetProperty("OrderID").GetValue(input));
            const int maxRecords = 500;

            List<ClientInstructionsEquityOutput> outputList = new List<ClientInstructionsEquityOutput>();
            if (paging.PageNumber<1 || paging.PageSize < 1)
            {
                paging.PageNumber = 1;
                paging.PageSize = maxRecords;
            }
           
            int startRecord = (paging.PageNumber - 1) * paging.PageSize + 1;
            int endRecord = paging.PageNumber * paging.PageSize;
            if (endRecord > maxRecords)
            {
                endRecord = maxRecords;
            }
            await Task.Run(() =>
            {
                for (int i = startRecord; i <= endRecord; i++)
                {
                    ClientInstructionsEquityOutput dbRecord = new ClientInstructionsEquityOutput()
                    {
                        AFFI_NAFFI = "AFFI_NAFFI M" + i,
                        AppRef = "AppRef M" + i,
                        AssetFamily = "AppRef M" + i,
                        BuyerRef = "BayerRef M" + i,
                        BeingCancel = true,
                        BuyerAccount = "BuyerAccount M" + i,
                        CancelManually = true,
                        Client = "Client M" + i,
                        ClientInstStatus = "ClientInstStatus M" + i,
                        ClientRef = "ClientRef M" + i,
                        Committed = true,
                        Counterpart = "Counterpart M" + i,
                        CP_BTB = "CP_BTB M" + i,
                        CP_REF = "CP_REF M" + i,
                        CreationDate = DateTime.Now,
                        CreatorUserID = "CreatorUserID M" + i,
                        DependencyLink = "DependencyLink M" + i,
                        Description = "Description M" + i,
                        FI = "FI M" + i,
                        GROUP_REF = "GROUP_REF M" + i,
                        IntMatchStatus = "IntMatchStatus M" + i,
                        ISIN = "ISIN M" + i,
                        JSEStatus = "JSEStatus M" + i,
                        LastNotificationToClient = "LastNotificationToClient M" + i,
                        LastNotificationToJSE = "LastNotificationToJSE M" + i,
                        LinkedRef = "LinkedRef M" + i,
                        LinkType = "LinkType M" + i,
                        ManualInput = true,
                        Market = "Market M" + i,
                        NostroSecAcc = "NostroSecAcc M" + i,
                        NotificationType = "NotificationType M" + i,
                        OffEx = "OffEx M" + i,
                        OrderID = "OrderID M - " + i,
                        OrderQuantity = i,
                        PaymentReference = "PaymentReference M" + i,
                        Processed = true,
                        Quantity = i,
                        SecAcc = "SecAcc M" + i,
                        SellerAccount = "SellerAccount M" + i,
                        SellerRef = "SellerRef M" + i,
                        SetAmount = i,
                        SettDate = DateTime.Now,
                        SettDiff = "SettDiff M" + i,
                        SubStatus = "SubStatus M" + i,
                        Taxable = "true",
                        TradeDate = DateTime.Now,
                        TransType = "TransType M" + i,
                        TransTypeDescription = "TransTypeDescription M" + i,
                        UpdateDate = DateTime.Now,
                        UpdateUserID = "UpdateUserID M" + i
                    };
                    outputList.Add(dbRecord);

                }



            });
   
            return outputList;

        }

        public async Task<long> CountClientInstructionsEquityAsync(ClientInstructionsEquityParams input)
        {
            long countRecords = 0;
            await Task.Run(() =>
            {
                countRecords = 500;
            });
            return countRecords;
        }

        public async Task<List<ClientInstructionsBondsOutput>> GetClientInstructionsBondsAsync(ClientInstructionsBondsParams input, DataPagingInput paging)
        {
            var oID_value = (input.GetType().GetProperty("OrderID").GetValue(input) == null ? " N U L L " : input.GetType().GetProperty("OrderID").GetValue(input));
            const int maxRecords = 500;

            List<ClientInstructionsBondsOutput> outputList = new List<ClientInstructionsBondsOutput>();
            if (paging.PageNumber < 1 || paging.PageSize < 1)
            {
                paging.PageNumber = 1;
                paging.PageSize = maxRecords;
            }

            int startRecord = (paging.PageNumber - 1) * paging.PageSize + 1;
            int endRecord = paging.PageNumber * paging.PageSize;
            if (endRecord > maxRecords)
            {
                endRecord = maxRecords;
            }
            await Task.Run(() =>
            {
                for (int i = startRecord; i <= endRecord; i++)
                {
                    ClientInstructionsBondsOutput dbRecord = new ClientInstructionsBondsOutput()
                    {
                        AFFI_NAFI = "AFFI_NAFFI M" + i,
                        AppRef = "AppRef M" + i,
                        ClientInstStatus = "ClientInstStatus M" + i,
                        BuyerRef = "BayerRef M" + i,
                        BeingCancel = "true",
                        BuyerAccount = "BuyerAccount M" + i,
                        CancelManually = "true",
                        Cpty = "Cpty M" + i,
                        MktInstStatus = "MktInstStatus M" + i,
                        ClientRef = "ClientRef M" + i,
                        Committed = "true",
                        SettAmount = i,
                        CP_BTB = "CP_BTB M" + i,
                        CP_REF = "CP_REF M" + i,
                        CreationDate = DateTime.Now,
                        CreatorUserID = "CreatorUserID M" + i,
                        DependencyLink = "DependencyLink M" + i,
                        Description = "Description M" + i,
                        FI = "FI M" + i,
                        GROUP_REF = "GROUP_REF M" + i,
                        IntMatchStatus = "IntMatchStatus M" + i,
                        ISIN = "ISIN M" + i,
                        LastNotificationToClient = "LastNotificationToClient M" + i,
                        LastNotificationToJSE = "LastNotificationToJSE M" + i,
                        LinkedRef = "LinkedRef M" + i,
                        LinkType = "LinkType M" + i,
                        ManualInput = "true",
                        NostroSecAcc = "NostroSecAcc M" + i,
                        NotificationType = "NotificationType M" + i,
                        OffEx = "OffEx M" + i,
                        OrderID = "OrderID M - " + oID_value,
                        PaymentReference = "PaymentReference M" + i,
                        Processed = "true",
                        Quantity = i,
                        SecAcc = "SecAcc M" + i,
                        SellerAccount = "SellerAccount M" + i,
                        SellerRef = "SellerRef M" + i,
                        SettDate = DateTime.Now,
                        SettDiff = "SettDiff M" + i,
                        SubStatus = "SubStatus M" + i,
                        Taxable = "true",
                        TradeDate = DateTime.Now,
                        TransType = "TransType M" + i,
                        TransTypeDescription = "TransTypeDescription M" + i,
                        UpdateDate = DateTime.Now,
                        UpdateUserID = "UpdateUserID M" + i
                    };
                    outputList.Add(dbRecord);

                }



            });

            return outputList;
        }

        public async Task<long> CountClientInstructionsBondsAsync(ClientInstructionsBondsParams input)
        {
            long countRecords = 0;
            await Task.Run(() =>
            {
                countRecords = 500;
            });
            return countRecords;
        }

        public async Task<List<ClientInstructionsMoneyMarketOutput>> GetClientInstructionsMoneyMarketAsync(ClientInstructionsMoneyMarketParams input, DataPagingInput paging)
        {
            const int maxRecords = 500;

            List<ClientInstructionsMoneyMarketOutput> outputList = new List<ClientInstructionsMoneyMarketOutput>();
            if (paging.PageNumber < 1 || paging.PageSize < 1)
            {
                paging.PageNumber = 1;
                paging.PageSize = maxRecords;
            }

            int startRecord = (paging.PageNumber - 1) * paging.PageSize + 1;
            int endRecord = paging.PageNumber * paging.PageSize;
            if (endRecord > maxRecords)
            {
                endRecord = maxRecords;
            }
            await Task.Run(() =>
            {
                for (int i = startRecord; i <= endRecord; i++)
                {
                    ClientInstructionsMoneyMarketOutput dbRecord = new ClientInstructionsMoneyMarketOutput()
                    {
                        Cpty = "Cpty M" + i,
                        AppRef = "AppRef M" + i,
                        MktInstStatus = "MktInstStatus M" + i,
                        BuyerRef = "BuyerRef M" + i,
                        BeingCancel = true,
                        CancelManually = true,
                        ClientInstStatus = "ClientInstStatus M" + i,
                        ClientRef = "ClientRef M" + i,
                        Committed = true,
                        CreationDate = DateTime.Now,
                        CreatorUserID = "CreatorUserID M" + i,
                        Description = "Description M" + i,
                        FI = "FI M" + i,
                        IntMatchStatus = "IntMatchStatus M" + i,
                        ISIN = "ISIN M" + i,
                        LastNotificationToClient = "LastNotificationToClient M" + i,
                        LinkedRef = "LinkedRef M" + i,
                        ManualInput = true,
                        NostroSecAcc = "NostroSecAcc M" + i,
                        NotificationType = "NotificationType M" + i,
                        OffEx = "OffEx M" + i,
                        OrderID = "OrderID M - " + i,
                        Processed = true,
                        Quantity = i,
                        SecAcc = "SecAcc M" + i,
                        SellerRef = "SellerRef M" + i,
                        SetAmount = i,
                        SettDate = DateTime.Now,
                        SettDiff = "SettDiff M" + i,
                        SubStatus = "SubStatus M" + i,
                        Taxable = "true",
                        TradeDate = DateTime.Now,
                        TransType = "TransType M" + i,
                        TransTypeDescription = "TransTypeDescription M" + i,
                        UpdateDate = DateTime.Now,
                        UpdateUserID = "UpdateUserID M" + i
                    };
                    outputList.Add(dbRecord);

                }



            });

            return outputList;
        }

        public async Task<long> CountClientInstructionsMoneyMarketAsync(ClientInstructionsMoneyMarketParams input)
        {
            long countRecords = 0;
            await Task.Run(() =>
            {
                countRecords = 500;
            });
            return countRecords;
        }

        public async Task<List<ClientPositionOutput>> GetClientPositionAsync(ClientPositionParams input, DataPagingInput paging)
        {
            const int maxRecords = 500;

            List<ClientPositionOutput> outputList = new List<ClientPositionOutput>();
            if (paging.PageNumber < 1 || paging.PageSize < 1)
            {
                paging.PageNumber = 1;
                paging.PageSize = maxRecords;
            }

            int startRecord = (paging.PageNumber - 1) * paging.PageSize + 1;
            int endRecord = paging.PageNumber * paging.PageSize;
            if (endRecord > maxRecords)
            {
                endRecord = maxRecords;
            }
            await Task.Run(() =>
            {
                for (int i = startRecord; i <= endRecord; i++)
                {
                    ClientPositionOutput dbRecord = new ClientPositionOutput()
                    {
                        AppRef = "AppRef" + i,
                        AssetFamily = "AssetFamily M" + i,
                        Depo = "Depo M" + i,
                        Custodian = "Custodian M" + i,
                        MainReference = "MainReference M" + i,
                        PositionDepoId = "PositionDepoId M" + i,
                        PositionDate = DateTime.Now,
                        QuantityType = "QuantityType M" + i,
                        PositionIdentifier = "PositionIdentifier M" + i,
                        PositionNature = "PositionNature M" + i,
                        ValidityDate = DateTime.Now,
                        TradableAsset = "TradableAsset M" + i,                        
                        Quantity = i,
                        SecAcc = "SecAcc M" + i
                    };
                    outputList.Add(dbRecord);
                }
            });

            return outputList;
        }

        public async Task<long> CountClientPositionAsync(ClientPositionParams input)
        {
            long countRecords = 0;
            await Task.Run(() =>
            {
                countRecords = 500;
            });
            return countRecords;
        }

        public async Task<List<DematRematOperationsOutput>> DematRematOperationsAsync(DematRematOperationsParams input, DataPagingInput paging)
        {
            var oID_value = (input.GetType().GetProperty("OrderID").GetValue(input) == null ? " N U L L " : input.GetType().GetProperty("OrderID").GetValue(input));
            const int maxRecords = 500;

            List<DematRematOperationsOutput> outputList = new List<DematRematOperationsOutput>();
            if (paging.PageNumber < 1 || paging.PageSize < 1)
            {
                paging.PageNumber = 1;
                paging.PageSize = maxRecords;
            }

            int startRecord = (paging.PageNumber - 1) * paging.PageSize + 1;
            int endRecord = paging.PageNumber * paging.PageSize;
            if (endRecord > maxRecords)
            {
                endRecord = maxRecords;
            }
            await Task.Run(() =>
            {
                for (int i = startRecord; i <= endRecord; i++)
                {
                    DematRematOperationsOutput dbRecord = new DematRematOperationsOutput()
                    {
                        ElectronicLegStatus = "ElectronicLegStatus M" + i,
                        AppRef = "AppRef M" + i,
                        LastNotifIn = "LastNotifIn M" + i,
                        PhysicalLegStatus = "PhysicalLegStatus M" + i,
                        BeingCancel = "true",
                        ReceiverAddress = "ReceiverAddress M" + i,
                        CancelManually = "true",
                        ReceiverName = "ReceiverName M" + i,
                        ClientRef = "ClientRef M" + i,
                        CreationDate = DateTime.Now,
                        CreatorUserID = "CreatorUserID M" + i,
                        FI = "FI M" + i,
                        OrderID = "OrderID M - " + oID_value,
                        Quantity = i,
                        SecAcc = "SecAcc M" + i,
                        SettDate = DateTime.Now,
                        TransType = "TransType M" + i,
                        UpdateDate = DateTime.Now,
                        UpdateUserID = "UpdateUserID M" + i
                    };
                    outputList.Add(dbRecord);

                }



            });

            return outputList;
        }

        public async Task<long> CountDematRematOperationsAsync(DematRematOperationsParams input)
        {
            long countRecords = 0;
            await Task.Run(() =>
            {
                countRecords = 500;
            });
            return countRecords;
        }
    }
}