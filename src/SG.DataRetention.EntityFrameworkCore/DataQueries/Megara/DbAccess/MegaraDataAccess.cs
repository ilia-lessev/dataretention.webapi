﻿using Oracle.ManagedDataAccess.Client;
using SG.DataRetention.DataQueries.Common;
using SG.DataRetention.DataQueries.Common.Dto;
using SG.DataRetention.DataQueries.Megara.DbAccess;
using SG.DataRetention.DataQueries.Megara.Dto;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace SG.DataRetention.DataQueries.Megara
{

    public class MegaraDataAccess : IMegaraDataAccess
    {
        private string _dbConnectionString;

        private readonly string _baseQuery_ListClientInstructionsEquity = "SELECT fs.* FROM" +
                            Environment.NewLine + "(" +
                            Environment.NewLine + "select T_lastMarketInst_.linkRef_ OrderID, T_.clientReference_ ClientRef, T_.appReference_ AppRef, T_.clientSecAccount$code_ SecAcc, T_.transactionType$code_ TransType, T_.incomingQuantity_ Quantity, T_.fI$code_ FI, T_.offshore_ OffEx, T_.counterpart$code_ Counterpart, T_.businessStatus_ ClientInstStatus, T_.nostroSecAccount$code_ NostroSecAcc, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.tradeDate_ / 86400000) as TradeDate,(TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.tradeDate_ / 86400000) as TradeDateEnd, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.settlementDate_ / 86400000) as SettDate, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.settlementDate_ / 86400000) as SettDateEnd,T_.applyTax_ Taxable, T_lastMarketInst_.jSEStatus_ JSEStatus, T_.paymentReference_ PaymentReference, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.creationDate_ / 86400000) as CreationDate, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.creationDate_ / 86400000) as CreationDateEnd, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.updateDate_ / 86400000) as UpdateDate, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.updateDate_ / 86400000) as UpdateDateEnd, T_tradableAsset_.assetFamily_ AssetFamily, T_.creatorUserId_ CreatorUserID, T_.updatorUserId_  UpdateUserID," +
                            Environment.NewLine + "T_.settlementAmount_ SetAmount, T_.buyerAccount_ BuyerAccount, T_.sellerAccount_ SellerAccount, T_lastMarketInst_.businessStatus_ MktInstStatus, T_lastMarketInst_.lastAffirmationStatus_ AFFI_NAFFI, T_.cPBTBReference_ CP_BTB, T_.cPSAFReference_ CP_REF, T_.groupReference_ GROUP_REF, T_linkedInstruction_.linkType_ LinkType, T_linkedInstruction_.dependencyLink$code_ DependencyLink, T_.lastNotificationToClient$code_ LastNotificationToClient, T_lastMarketInst_.lastNotificationToJSE$code_ LastNotificationToJSE,  T_lastMark_1LYPCMG_cationType_.description_ Description, T_.iSIN_ ISIN, T_.beingCancelled_ BeingCancel, T_.cancelledManually_ CancelManually, T_.committed_ Committed,  T_transactionType_.description_ TransTypeDescription, T_lastMark_1LYPCMG_cationType_.identifier_ NotificationType, T_.buyerReference_ BuyerRef, T_.sellerReference_ SellerRef, T_.internalMatchingStatus_ IntMatchStatus, T_.processed_ Processed, T_.manualInput_ ManualInput" +
                            Environment.NewLine + "from inst_SimpleClientInstruction_ T_" +
                            Environment.NewLine + "left join ds_WebClientInputNotification_ T_lastNotifOut_ on (T_.lastNotifOut$pk_ = T_lastNotifOut_.pk_)" +
                            Environment.NewLine + "left join fi_OtherTradableAsset_ T_tradableAsset_ on(T_.tradableAsset$pk_ = T_tradableAsset_.pk_)" +
                            Environment.NewLine + "left join dnstruction_LinkedInstruction_ T_linkedInstruction_ on(T_.linkedInstruction$pk_ = T_linkedInstruction_.pk_)" +
                            Environment.NewLine + "left join inst_TransactionType_ T_transactionType_ on(T_.transactionType$pk_ = T_transactionType_.pk_)" +
                            Environment.NewLine + "left join dmci_MarketInstruction_ T_lastMarketInst_ on(T_.lastMarketInst$pk_ = T_lastMarketInst_.pk_)" +
                            Environment.NewLine + "left join dotification_NotificationType_ T_lastMark_1LYPCMG_cationType_ on(T_lastMarketInst_.lastNotificationType$pk_ = T_lastMark_1LYPCMG_cationType_.pk_)" +
                            Environment.NewLine + "left join inst_AdditionalData_ T_additionalData_ on(T_.additionalData$pk_ = T_additionalData_.pk_) " +
                            Environment.NewLine + "where (T_.transactionType$code_ not in ('ALLOTMENT_RFP', 'ALLOTMENT_DFP') or T_.transactionType$code_ is null) " +
                            Environment.NewLine + "and T_.type_ in ('com.vermeg.mCusSACM.inst.SCIEquities', 'com.vermeg.mCusSACM.dematRemat.SCIOtherForDemat', 'com.vermeg.mCusSACM.inst.SCIAllotment', 'com.vermeg.mCusSACM.unlistedShare.SCIForUnlistedEquities', 'com.vermeg.mCusSACM.dematRemat.SCIEquitiesForRemat', 'com.vermeg.mCusSACM.dematRemat.SCIOtherForRemat', 'com.vermeg.mCusSACM.dematRemat.SCIEquitiesForDemat')" +
                            Environment.NewLine + ") fs";

        private readonly string _baseQuery_ListClientInstructionsMoneyMarket = "SELECT fs.* FROM" +
                            Environment.NewLine + "(" +
                            Environment.NewLine + "select  T_lastMarketInst_.linkRef_ OrderID, T_.clientSecAccount$code_ SecAcc, T_.transactionType$code_ TransType, T_.incomingQuantity_ Quantity, T_.fI$code_ FI, T_.settlementAmount_ SetAmount, T_.counterpart$code_ Counterpart, T_.counterpart$code_ Cpty, T_.buyerReference_ BuyerRef, T_.sellerReference_ SellerRef, T_.applyTax_ Taxable, T_.clientReference_ ClientRef, T_.businessStatus_ ClientInstStatus, " +
                            Environment.NewLine + "T_.appReference_ AppRef, T_.iSIN_ ISIN, T_.offshore_ OffEx, T_.beingCancelled_ BeingCancel, T_.cancelledManually_ CancelManually, T_.internalMatchingStatus_ IntMatchStatus, T_.processed_ Processed, T_.nostroSecAccount$code_ NostroSecAcc, T_.updatorUserId_  UpdateUserID, T_.creatorUserId_ CreatorUserID, T_.manualInput_ ManualInput, T_.committed_ Committed, T_.lastNotificationToClient$code_ LastNotificationToClient," +
                            Environment.NewLine + "(TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.tradeDate_ / 86400000) as TradeDate,(TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.tradeDate_ / 86400000) as TradeDateEnd, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.settlementDate_ / 86400000) as SettDate, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.settlementDate_ / 86400000) as SettDateEnd," +
                            Environment.NewLine + "(TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.updateDate_ / 86400000) as UpdateDate, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.updateDate_ / 86400000) as UpdateDateEnd, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.creationDate_ / 86400000) as CreationDate, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.creationDate_ / 86400000) as CreationDateEnd, " +
                            Environment.NewLine + "T_lastMark_1LYPCMG_cationType_.identifier_ NotificationType, T_lastMark_1LYPCMG_cationType_.description_ Description, T_transactionType_.description_ TransTypeDescription" +
                            Environment.NewLine + "from inst_SimpleClientInstruction_ T_ " +
                            Environment.NewLine + "left join ds_WebClientInputNotification_ T_lastNotifOut_ on (T_.lastNotifOut$pk_ = T_lastNotifOut_.pk_) " +
                            Environment.NewLine + "left join inst_TransactionType_ T_transactionType_ on (T_.transactionType$pk_ = T_transactionType_.pk_) " +
                            Environment.NewLine + "left join dmci_MarketInstruction_ T_lastMarketInst_ on (T_.lastMarketInst$pk_ = T_lastMarketInst_.pk_) " +
                            Environment.NewLine + "left join dotification_NotificationType_ T_lastMark_1LYPCMG_cationType_ on (T_lastMarketInst_.lastNotificationType$pk_ = T_lastMark_1LYPCMG_cationType_.pk_) " +
                            Environment.NewLine + "left join inst_AdditionalData_ T_additionalData_ on (T_.additionalData$pk_ = T_additionalData_.pk_) " +
                            Environment.NewLine + "where T_.type_ in ('com.vermeg.mCusSACM.inst.ElecMMSCI') " +
                            Environment.NewLine + ") fs";

        private readonly string _baseQuery_ListClientInstructionsBonds = "SELECT fs.* FROM" +
                           Environment.NewLine + "(" +
                           Environment.NewLine + "select T_.offshore_ OffEx,T_.applyTax_ Taxable,T_.appReference_ AppRef, T_.beingCancelled_ BeingCancel, T_.businessStatus_ ClientInstStatus, T_.buyerAccount_ BuyerAccount, T_.buyerReference_ BuyerRef, T_.cPBTBReference_ CP_BTB, T_.cPSAFReference_ CP_REF, T_.cancelledManually_ CancelManually, T_.clientReference_ ClientRef, T_.clientSecAccount$code_ SecAcc, T_.committed_ Committed, T_.counterpart$code_ Cpty, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.creationDate_ / 86400000) as CreationDate, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.creationDate_ / 86400000) as CreationDateEnd, T_.creatorUserId_ CreatorUserId, T_.fI$code_ FI, T_.fI$pk_, T_.groupReference_ GROUP_REF, T_.iSIN_ ISIN, T_.incomingQuantity_ Quantity, T_.internalMatchingStatus_ IntMatchStatus, T_lastMarketInst_.lastAffirmationStatus_ AFFI_NAFFI, T_lastMarketInst_.lastNotificationToJSE$code_ LastNotificationToJSE, T_lastMark_1LYPCMG_cationType_.description_ Description, T_lastMark_1LYPCMG_cationType_.identifier_ NotificationType, T_lastMarketInst_.linkRef_ OrderID, T_.lastNotificationToClient$code_ LastNotificationToClient , T_linkedInstruction_.dependencyLink$code_ DependencyLink,T_linkedInstruction_.linkType_ LinkType, T_.manualInput_ ManualInput, T_.nostroSecAccount$code_ NostroSecAcc, T_.paymentReference_ PaymentReference, T_.processed_ Processed, T_.sellerAccount_ SellerAccount, T_.sellerReference_ SellerRef, T_.settlementAmount_ SettAmount, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.settlementDate_ / 86400000) as SettDate, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.settlementDate_ / 86400000) as SettDateEnd,T_.toleratedAmount_, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.tradeDate_ / 86400000) as TradeDate, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.tradeDate_ / 86400000) as TradeDateEnd, T_.transactionType$code_ TransType,T_transactionType_.description_ TransTypeDescription, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.updateDate_ / 86400000) as UpdateDate, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.updateDate_ / 86400000) as UpdateDateEnd, T_.updatorUserId_ UpdateUserId" +
                           Environment.NewLine + "from inst_SimpleClientInstruction_ T_" +
                           Environment.NewLine + "left" +
                           Environment.NewLine + "join ds_WebClientInputNotification_ T_lastNotifOut_ on (T_.lastNotifOut$pk_ = T_lastNotifOut_.pk_)" +
                           Environment.NewLine + "left join dnstruction_LinkedInstruction_ T_linkedInstruction_ on(T_.linkedInstruction$pk_ = T_linkedInstruction_.pk_)" +
                           Environment.NewLine + "left join inst_TransactionType_ T_transactionType_ on(T_.transactionType$pk_ = T_transactionType_.pk_)" +
                           Environment.NewLine + "left join dmci_MarketInstruction_ T_lastMarketInst_ on(T_.lastMarketInst$pk_ = T_lastMarketInst_.pk_)" +
                           Environment.NewLine + "left join dotification_NotificationType_ T_lastMark_1LYPCMG_cationType_ on(T_lastMarketInst_.lastNotificationType$pk_ = T_lastMark_1LYPCMG_cationType_.pk_)" +
                           Environment.NewLine + "left join inst_AdditionalData_ T_additionalData_ on(T_.additionalData$pk_ = T_additionalData_.pk_)" +
                           Environment.NewLine + "where T_.settlementCurrency$code_ = 'ZAR'" +
                           Environment.NewLine + "and T_.type_ in ('com.vermeg.mCusSACM.inst.SCIBonds', 'com.vermeg.mCusSACM.dematRemat.SCIBondsForDemat', 'com.vermeg.mCusSACM.unlistedShare.SCIUnlistedBonds', 'com.vermeg.mCusSACM.inst.SCIIssuerTrade', 'com.vermeg.mCusSACM.dematRemat.SCIBondsForRemat')" +
                           Environment.NewLine + ") fs";

        private readonly string _baseQuery_ClientPosition = "SELECT fs.*FROM" +
                            Environment.NewLine + "(" +
                            Environment.NewLine + "select T_.appReference_ AppRef, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.creationDate_ / 86400000) as CreationDate, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.creationDate_ / 86400000) as CreationDateEnd, T_.depo$code_ Depo, T_.mainReference_ MainReference, T_.owner$code_ SecAcc, T_.pk_ PK, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.positionDate_ / 86400000) as PositionDate, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.positionDate_ / 86400000) as PositionDateEnd, T_.positionDepoId_ PositionDepoId, T_.positionIdentifier_ PositionIdentifier, T_.positionNature$code_ PositionNature, T_.quantity_ Quantity, T_.quantityType_ QuantityType, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.updateDate_ / 86400000) as UpdateDate, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.updateDate_ / 86400000) as UpdateDateEnd, (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.validityDate_ / 86400000) ValidityDate, T_.counterpart$code_ Counterpart, T_.impacted_ Impacted, T_.processed_ Processed, T_.tradableAsset$code_ TradableAsset, " +
                            Environment.NewLine + "T_depo_.custodian$code_ Custodian, T_tradableAsset_.assetFamily_ AssetFamily" +
                            Environment.NewLine + "from bpos_SecAccountRecord_ T_" +
                            Environment.NewLine + "left join acc_NostroSecAccount_ T_depo_ on (T_.depo$pk_ = T_depo_.pk_)" +
                            Environment.NewLine + "left join fi_SecurityByMarket_ T_tradableAsset_ on(T_.tradableAsset$pk_ = T_tradableAsset_.pk_)" +
                            Environment.NewLine + "left join fi_Security_ T_tradableAsset_security_ on(T_tradableAsset_.security$pk_ = T_tradableAsset_security_.pk_)" +
                            Environment.NewLine + "where (T_.type_ in ('com.vermeg.services.businessPosition.constraintsModel.Position')) " +
                            Environment.NewLine + ") fs";

        string _baseQuery_DematRematOperation = "SELECT fs.* FROM" +
                Environment.NewLine + "(" +
                Environment.NewLine + "Select (TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_physicalLeg_.tradeDate_ / 86400000) TradeDate,"+
                Environment.NewLine + "(TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_physicalLeg_.tradeDate_ / 86400000) TradeDateEnd," +
                Environment.NewLine + "(TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_physicalLeg_.creationDate_ / 86400000) CreationDate," +
                Environment.NewLine + "(TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_physicalLeg_.creationDate_ / 86400000) CreationDateEnd," +
                Environment.NewLine + "(TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.updateDate_ / 86400000) UpdateDate," +
                Environment.NewLine + "(TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_.updateDate_ / 86400000) UpdateDateEnd," +
                Environment.NewLine + "(TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_physicalLeg_.settlementDate_ / 86400000) SettDate," +
                Environment.NewLine + "(TO_DATE('1970-01-01', 'YYYY-MM-DD') + T_physicalLeg_.settlementDate_ / 86400000) SettDateEnd," +
                Environment.NewLine + "T_physicalLeg_.cancelledManually_ CancelManually," +
                Environment.NewLine + "T_physicalLeg_.offshore_ OffEx," +
                Environment.NewLine + "T_physicalLeg_.nostroSecAccount$code_ NostroSecAcc," +
                Environment.NewLine + "T_physicalLeg_.applyTax_ Taxable," +
                Environment.NewLine + "T_.appReference_ AppRef, T_.creatorUserId_ CreatorUserId," +
                Environment.NewLine + "T_electronicLeg_.businessStatus_ ElectronicLegStatus," +
                Environment.NewLine + "T_electroni_SBOGF_tMarketInst_.linkRef_ OrderID," +
                Environment.NewLine + "T_electronicLeg_.lastNotifIn$code_ LastNotifIn," +
                Environment.NewLine + "T_physicalLeg_.beingCancelled_ BeingCancel, T_physicalLeg_.businessStatus_ PhysicalLegStatus," +
                Environment.NewLine + "T_physicalLeg_.clientReference_ ClientRef, T_physicalLeg_.clientSecAccount$code_ SecAcc," +
                Environment.NewLine + "T_physicalLeg_.fI$code_ FI," +
                Environment.NewLine + "T_physicalLeg_.incomingQuantity_ Quantity," +
                Environment.NewLine + "T_physicalLeg_.transactionType$code_ TransType," +
                Environment.NewLine + "T_.receiverAddress_ ReceiverAddress, T_.receiverName_ ReceiverName, T_.updatorUserId_ UpdateUserId " +
                Environment.NewLine + "from ddematRemat_DematRemat_ T_" +
                Environment.NewLine + "inner join inst_SimpleClientInstruction_ T_physicalLeg_ on(T_.physicalLeg$pk_ = T_physicalLeg_.pk_)" +
                Environment.NewLine + "left join inst_SimpleClientInstruction_ T_electronicLeg_ on(T_.electronicLeg$pk_ = T_electronicLeg_.pk_)" +
                Environment.NewLine + "left join dmci_MarketInstruction_ T_electroni_SBOGF_tMarketInst_ on(T_electronicLeg_.lastMarketInst$pk_ = T_electroni_SBOGF_tMarketInst_.pk_)" +
                Environment.NewLine + "where T_physicalLeg_.transactionType$code_ in ('DEMAT', 'REMAT')" +
                Environment.NewLine + ") fs";


        public MegaraDataAccess(string dbConnectionString)
        {
            _dbConnectionString = dbConnectionString;
        }
        
        public async Task<List<ClientInstructionsEquityOutput>> GetClientInstructionsEquityAsync(ClientInstructionsEquityParams input, DataPagingInput paging)
        {            
            List<ClientInstructionsEquityOutput> outputList = new List<ClientInstructionsEquityOutput>();

            using (OracleConnection con = new OracleConnection(_dbConnectionString))
            {
                con.Open();
                using (OracleCommand cmd = DBHelper<ClientInstructionsEquityParams>.BuildCommandDynamicFilter(input, paging, _baseQuery_ListClientInstructionsEquity, PossibleParameters.ListClientInstructionsEquity))
                {
                    cmd.Connection = con;
                    OracleDataReader reader = (OracleDataReader)await cmd.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        ClientInstructionsEquityOutput dbRecord = new ClientInstructionsEquityOutput()
                        {
                            AFFI_NAFFI = reader["AFFI_NAFFI"].ToString(),
                            AppRef = reader["AppRef"].ToString(),
                            AssetFamily = reader["AssetFamily"].ToString(),
                            BuyerRef = reader["BuyerRef"].ToString(),
                            BeingCancel = Boolean.Parse(reader["BeingCancel"].ToString()),
                            BuyerAccount = reader["BuyerAccount"].ToString(),
                            CancelManually = Boolean.Parse(reader["CancelManually"].ToString()),
                            //Client = reader["Client"].ToString(),
                            ClientInstStatus = reader["ClientInstStatus"].ToString(),
                            ClientRef = reader["ClientRef"].ToString(),
                            Committed = Boolean.Parse(reader["Committed"].ToString()),
                            Counterpart = reader["Counterpart"].ToString(),
                            CP_BTB = reader["CP_BTB"].ToString(),
                            CP_REF = reader["CP_REF"].ToString(),
                            CreationDate = Convert.ToDateTime(reader["CreationDate"].ToString()),
                            CreatorUserID = reader["CreatorUserID"].ToString(),
                            DependencyLink = reader["DependencyLink"].ToString(),
                            Description = reader["Description"].ToString(),
                            FI = reader["FI"].ToString(),
                            GROUP_REF = reader["GROUP_REF"].ToString(),
                            IntMatchStatus = reader["IntMatchStatus"].ToString(),
                            ISIN = reader["ISIN"].ToString(),
                            JSEStatus = reader["JSEStatus"].ToString(),
                            LastNotificationToClient = reader["LastNotificationToClient"].ToString(),
                            LastNotificationToJSE = reader["LastNotificationToJSE"].ToString(),
                            //LinkedRef = reader["LinkedRef"].ToString(),
                            LinkType = reader["LinkType"].ToString(),
                            ManualInput = Boolean.Parse(reader["ManualInput"].ToString()),
                            //Market = reader["Market"].ToString(),
                            MktInstStatus = reader["MktInstStatus"].ToString(),
                            NostroSecAcc = reader["NostroSecAcc"].ToString(),
                            NotificationType = reader["NotificationType"].ToString(),
                            OffEx = reader["OffEx"].ToString(),
                            OrderID = reader["OrderID"].ToString(),
                            //OrderQuantity = Convert.ToInt32(reader["OrderQuantity"].ToString()),
                            PaymentReference = reader["PaymentReference"].ToString(),
                            Processed = Boolean.Parse(reader["Processed"].ToString()),
                            Quantity = Convert.ToInt32(reader["Quantity"].ToString()),
                            SecAcc = reader["SecAcc"].ToString(),
                            SellerAccount = reader["SellerAccount"].ToString(),
                            SellerRef = reader["SellerRef"].ToString(),
                            SetAmount = Decimal.Parse(reader["SetAmount"].ToString()),
                            SettDate = DateTime.Parse(reader["SettDate"].ToString()),
                            //SettDiff = reader["asd"].ToString(),
                            //SubStatus = reader["SubStatus"].ToString(),
                            Taxable = reader["Taxable"].ToString(),
                            TradeDate = Convert.ToDateTime(reader["TradeDate"].ToString()),
                            TransType = reader["TransType"].ToString(),
                            TransTypeDescription = reader["TransTypeDescription"].ToString(),
                            UpdateDate = Convert.ToDateTime(reader["UpdateDate"].ToString()),
                            UpdateUserID = reader["UpdateUserID"].ToString()

                        };
                        outputList.Add(dbRecord);
                    }                                           
                }
            }
            return outputList;
        }

        public async Task<long> CountClientInstructionsEquityAsync(ClientInstructionsEquityParams input)
        {           
            using (OracleConnection con = new OracleConnection(_dbConnectionString))
            {
                con.Open();
                using (OracleCommand cmd = DBHelper<ClientInstructionsEquityParams>.BuildCountCommandDynamicFilter(input, _baseQuery_ListClientInstructionsEquity, PossibleParameters.ListClientInstructionsEquity))
                {
                    cmd.Connection = con;
                    var recordsCount = await cmd.ExecuteScalarAsync();
                    long recordCountLong = Convert.ToInt64(recordsCount);
                    return recordCountLong;
                }
            }
        }

        public async Task<List<ClientInstructionsBondsOutput>> GetClientInstructionsBondsAsync(ClientInstructionsBondsParams input, DataPagingInput paging)
        {            
            List<ClientInstructionsBondsOutput> outputList = new List<ClientInstructionsBondsOutput>();

            using (OracleConnection con = new OracleConnection(_dbConnectionString))
            {
                con.Open();
                using (OracleCommand cmd = DBHelper<ClientInstructionsBondsParams>.BuildCommandDynamicFilter(input, paging, _baseQuery_ListClientInstructionsBonds, PossibleParameters.ListClientInstructionsBonds))
                {
                    cmd.Connection = con;
                    OracleDataReader reader = (OracleDataReader)await cmd.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        ClientInstructionsBondsOutput dbRecord = new ClientInstructionsBondsOutput()
                        {
                            AFFI_NAFI = reader["AFFI_NAFFI"].ToString(),
                            AppRef = reader["AppRef"].ToString(),
                            ClientInstStatus = reader["ClientInstStatus"].ToString(),
                            BuyerRef = reader["BuyerRef"].ToString(),
                            BeingCancel = reader["BeingCancel"].ToString(),
                            BuyerAccount = reader["BuyerAccount"].ToString(),
                            CancelManually = reader["CancelManually"].ToString(),
                            Cpty = reader["Cpty"].ToString(),
                            ClientRef = reader["ClientRef"].ToString(),
                            Committed = reader["Committed"].ToString(),
                            FI = reader["FI"].ToString(),
                            CP_BTB = reader["CP_BTB"].ToString(),
                            CP_REF = reader["CP_REF"].ToString(),
                            CreationDate = Convert.ToDateTime(reader["CreationDate"].ToString()),
                            CreatorUserID = reader["CreatorUserID"].ToString(),
                            DependencyLink = reader["DependencyLink"].ToString(),
                            Description = reader["Description"].ToString(),
                            GROUP_REF = reader["GROUP_REF"].ToString(),
                            IntMatchStatus = reader["IntMatchStatus"].ToString(),
                            ISIN = reader["ISIN"].ToString(),
                            //LinkedRef = reader["LinkedRef"].ToString(),
                            LastNotificationToClient = reader["LastNotificationToClient"].ToString(),
                            LastNotificationToJSE = reader["LastNotificationToJSE"].ToString(),
                            LinkType = reader["LinkType"].ToString(),
                            ManualInput = reader["ManualInput"].ToString(),
                            //MktInstStatus = reader["MktInstStatus"].ToString(),
                            NostroSecAcc = reader["NostroSecAcc"].ToString(),
                            NotificationType = reader["NotificationType"].ToString(),
                            OffEx = reader["OffEx"].ToString(),
                            OrderID = reader["OrderID"].ToString(),
                            PaymentReference = reader["PaymentReference"].ToString(),
                            Processed = reader["Processed"].ToString(),
                            Quantity = Convert.ToInt32(reader["Quantity"].ToString()),
                            SecAcc = reader["SecAcc"].ToString(),
                            SellerAccount = reader["SellerAccount"].ToString(),
                            SellerRef = reader["SellerRef"].ToString(),
                            SettAmount = Decimal.Parse(reader["SettAmount"].ToString()),
                            SettDate = DateTime.Parse(reader["SettDate"].ToString()),
                            Taxable = reader["Taxable"].ToString(),
                            TradeDate = Convert.ToDateTime(reader["TradeDate"].ToString()),
                            TransType = reader["TransType"].ToString(),
                            TransTypeDescription = reader["TransTypeDescription"].ToString(),
                            UpdateDate = Convert.ToDateTime(reader["UpdateDate"].ToString()),
                            UpdateUserID = reader["UpdateUserID"].ToString(),
                            //SettDiff = reader["SettDiff"].ToString(),
                            //SubStatus = reader["SubStatus"].ToString()
                        };
                        outputList.Add(dbRecord);
                    }                                    
                }

            }
            return outputList;
        }

        public async Task<long> CountClientInstructionsBondsAsync(ClientInstructionsBondsParams input)
        {
            using (OracleConnection con = new OracleConnection(_dbConnectionString))
            {
                con.Open();
                using (OracleCommand cmd = DBHelper<ClientInstructionsBondsParams>.BuildCountCommandDynamicFilter(input, _baseQuery_ListClientInstructionsBonds, PossibleParameters.ListClientInstructionsBonds))
                {
                    cmd.Connection = con;
                    var recordsCount = await cmd.ExecuteScalarAsync();
                    long recordCountLong = Convert.ToInt64(recordsCount);
                    return recordCountLong;
                }
            }
        }

        public async Task<List<ClientInstructionsMoneyMarketOutput>> GetClientInstructionsMoneyMarketAsync(ClientInstructionsMoneyMarketParams input, DataPagingInput paging)
        {
            
            List<ClientInstructionsMoneyMarketOutput> outputList = new List<ClientInstructionsMoneyMarketOutput>();

            using (OracleConnection con = new OracleConnection(_dbConnectionString))
            {
                con.Open();
                using (OracleCommand cmd = DBHelper<ClientInstructionsMoneyMarketParams>.BuildCommandDynamicFilter(input, paging, _baseQuery_ListClientInstructionsMoneyMarket, PossibleParameters.ListClientInstructionsMoneyMarket))
                {
                    cmd.Connection = con;
                    OracleDataReader reader = (OracleDataReader)await cmd.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        ClientInstructionsMoneyMarketOutput dbRecord = new ClientInstructionsMoneyMarketOutput()
                        {
                            Cpty = reader["Cpty"].ToString(),
                            AppRef = reader["AppRef"].ToString(),
                            FI = reader["FI"].ToString(),
                            BuyerRef = reader["BuyerRef"].ToString(),
                            BeingCancel = Boolean.Parse(reader["BeingCancel"].ToString()),
                            CancelManually = Boolean.Parse(reader["CancelManually"].ToString()),
                            ClientInstStatus = reader["ClientInstStatus"].ToString(),
                            ClientRef = reader["ClientRef"].ToString(),
                            Committed = Boolean.Parse(reader["Committed"].ToString()),
                            CreationDate = Convert.ToDateTime(reader["CreationDate"].ToString()),
                            CreatorUserID = reader["CreatorUserID"].ToString(),
                            Description = reader["Description"].ToString(),
                            IntMatchStatus = reader["IntMatchStatus"].ToString(),
                            ISIN = reader["ISIN"].ToString(),
                            LastNotificationToClient = reader["LastNotificationToClient"].ToString(),
                            ManualInput = Boolean.Parse(reader["ManualInput"].ToString()),
                            NostroSecAcc = reader["NostroSecAcc"].ToString(),
                            NotificationType = reader["NotificationType"].ToString(),
                            OffEx = reader["OffEx"].ToString(),
                            OrderID = reader["OrderID"].ToString(),
                            Processed = Boolean.Parse(reader["Processed"].ToString()),
                            Quantity = Convert.ToInt32(reader["Quantity"].ToString()),
                            SecAcc = reader["SecAcc"].ToString(),
                            SellerRef = reader["SellerRef"].ToString(),
                            SetAmount = Decimal.Parse(reader["SetAmount"].ToString()),
                            SettDate = DateTime.Parse(reader["SettDate"].ToString()),
                            Taxable = reader["Taxable"].ToString(),
                            TradeDate = Convert.ToDateTime(reader["TradeDate"].ToString()),
                            TransType = reader["TransType"].ToString(),
                            TransTypeDescription = reader["TransTypeDescription"].ToString(),
                            UpdateDate = Convert.ToDateTime(reader["UpdateDate"].ToString()),
                            UpdateUserID = reader["UpdateUserID"].ToString(),
                            //MktInstStatus = reader["MktInstStatus"].ToString(),
                            //SettDiff =  reader["SecAcc"].ToString(),
                            //SubStatus = reader["SubStatus"].ToString(),
                            //LinkedRef = reader["LinkedRef"].ToString(),
                            
                        };
                        outputList.Add(dbRecord);
                    }
                }

            }
            return outputList;
        }

        public async Task<long> CountClientInstructionsMoneyMarketAsync(ClientInstructionsMoneyMarketParams input)
        {
            using (OracleConnection con = new OracleConnection(_dbConnectionString))
            {
                con.Open();
                using (OracleCommand cmd = DBHelper<ClientInstructionsMoneyMarketParams>.BuildCountCommandDynamicFilter(input, _baseQuery_ListClientInstructionsMoneyMarket, PossibleParameters.ListClientInstructionsMoneyMarket))
                {
                    cmd.Connection = con;
                    var recordsCount = await cmd.ExecuteScalarAsync();
                    long recordCountLong = Convert.ToInt64(recordsCount);
                    return recordCountLong;                   
                }
            }
        }

        public async Task<List<ClientPositionOutput>> GetClientPositionAsync(ClientPositionParams input, DataPagingInput paging)
        {
            List<ClientPositionOutput> outputList = new List<ClientPositionOutput>();

            using (OracleConnection con = new OracleConnection(_dbConnectionString))
            {
                con.Open();
                using (OracleCommand cmd = DBHelper<ClientPositionParams>.BuildCommandDynamicFilter(input, paging, _baseQuery_ClientPosition, PossibleParameters.ListClientInstructionsClientPosition))
                {
                    cmd.Connection = con;
                    OracleDataReader reader = (OracleDataReader)await cmd.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        ClientPositionOutput dbRecord = new ClientPositionOutput()
                        {
                            AppRef = reader["AppRef"].ToString(),
                            AssetFamily = reader["AssetFamily"].ToString(),
                            MainReference = reader["MainReference"].ToString(),
                            Depo = reader["Depo"].ToString(),
                            Custodian = reader["Custodian"].ToString(),
                            PositionDate = DateTime.Parse(reader["PositionDate"].ToString()),
                            PositionDepoId = reader["PositionDepoId"].ToString(),
                            PositionIdentifier = reader["PositionIdentifier"].ToString(),
                            PositionNature = reader["PositionNature"].ToString(),
                            QuantityType = reader["QuantityType"].ToString(),
                            TradableAsset = reader["TradableAsset"].ToString(),
                            ValidityDate = DateTime.Parse(reader["ValidityDate"].ToString()),
                            Quantity = Convert.ToInt32(reader["Quantity"].ToString()),
                            SecAcc = reader["SecAcc"].ToString()
                        };
                        outputList.Add(dbRecord);
                    }
                }  

            }
            return outputList;
        }

        public async Task<long> CountClientPositionAsync(ClientPositionParams input)
        {           
            using (OracleConnection con = new OracleConnection(_dbConnectionString))
            {
                con.Open();
                using (OracleCommand cmd = DBHelper<ClientPositionParams>.BuildCountCommandDynamicFilter(input, _baseQuery_ClientPosition, PossibleParameters.ListClientInstructionsClientPosition))
                {
                    cmd.Connection = con;
                    var recordsCount = await cmd.ExecuteScalarAsync();
                    long recordCountLong = Convert.ToInt64(recordsCount);
                    return recordCountLong;

                }
            }
        }

        public async Task<List<DematRematOperationsOutput>> DematRematOperationsAsync(DematRematOperationsParams input, DataPagingInput paging)
        {
            List<DematRematOperationsOutput> outputList = new List<DematRematOperationsOutput>();

            using (OracleConnection con = new OracleConnection(_dbConnectionString))
            {
                con.Open();
                using (OracleCommand cmd = DBHelper<DematRematOperationsParams>.BuildCommandDynamicFilter(input, paging, _baseQuery_DematRematOperation, PossibleParameters.ListClientInstructionsListOperationsDematRemat))
                {
                    cmd.Connection = con;
                    OracleDataReader reader = (OracleDataReader)await cmd.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        DematRematOperationsOutput dbRecord = new DematRematOperationsOutput()
                        {
                            AppRef = reader["AppRef"].ToString(),
                            ElectronicLegStatus = reader["ElectronicLegStatus"].ToString(),
                            FI = reader["FI"].ToString(),
                            BeingCancel = reader["BeingCancel"].ToString(),
                            LastNotifIn = reader["LastNotifIn"].ToString(),
                            CancelManually = reader["CancelManually"].ToString(),
                            PhysicalLegStatus = reader["PhysicalLegStatus"].ToString(),
                            ClientRef = reader["ClientRef"].ToString(),
                            ReceiverAddress = reader["ReceiverAddress"].ToString(),
                            ReceiverName = reader["ReceiverName"].ToString(),
                            CreationDate = Convert.ToDateTime(reader["CreationDate"].ToString()),
                            CreatorUserID = reader["CreatorUserID"].ToString(),
                            OrderID = reader["OrderID"].ToString(),
                            Quantity = Convert.ToInt32(reader["Quantity"].ToString()),
                            SecAcc = reader["SecAcc"].ToString(),
                            SettDate = DateTime.Parse(reader["SettDate"].ToString()),
                            TransType = reader["TransType"].ToString(),
                            UpdateDate = Convert.ToDateTime(reader["UpdateDate"].ToString()),
                            UpdateUserID = reader["UpdateUserID"].ToString()
                        };
                        outputList.Add(dbRecord);
                    }                
                }

            }
            return outputList;
        }

        public async Task<long> CountDematRematOperationsAsync(DematRematOperationsParams input)
        {
            using (OracleConnection con = new OracleConnection(_dbConnectionString))
            {
                con.Open();
                using (OracleCommand cmd = DBHelper<DematRematOperationsParams>.BuildCountCommandDynamicFilter(input, _baseQuery_DematRematOperation, PossibleParameters.ListClientInstructionsListOperationsDematRemat))
                {
                    cmd.Connection = con;
                    var recordsCount = await cmd.ExecuteScalarAsync();
                    long recordCountLong = Convert.ToInt64(recordsCount);
                    return recordCountLong;
                }
            }
        }
    }
}
