﻿using Oracle.ManagedDataAccess.Client;
using SG.DataRetention.DataQueries.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SG.DataRetention.DataQueries.Megara.DbAccess
{
    public static class PossibleParameters
    {
        public readonly static Dictionary<string, IncomingParamDescriptor> ListClientInstructionsEquity = new Dictionary<string, IncomingParamDescriptor>()
        {
            {"OrderID",new IncomingParamDescriptor(){ParamName="OrderID",DbType=OracleDbType.Varchar2,IncomingType=Type.GetType("System.String"),FilterType=QueryFilterTypes.Standard}},
            {"ClientRef",new IncomingParamDescriptor(){ParamName="ClientRef",DbType=OracleDbType.Varchar2,IncomingType=Type.GetType("System.String"),FilterType=QueryFilterTypes.Standard}},
            {"AppRef",new IncomingParamDescriptor() { ParamName = "AppRef",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            ////{"Client",new IncomingParamDescriptor() { ParamName = "Client",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"SecAcc",new IncomingParamDescriptor() { ParamName = "SecAcc",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"TransType",new IncomingParamDescriptor() { ParamName = "TransType",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"FI",new IncomingParamDescriptor() { ParamName = "FI",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"Quantity",new IncomingParamDescriptor() { ParamName = "Quantity",DbType = OracleDbType.Int32,IncomingType = typeof(Nullable<Int32>),FilterType = QueryFilterTypes.Standard}},
            {"OffEx",new IncomingParamDescriptor() { ParamName = "OffEx",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"Counterpart",new IncomingParamDescriptor() { ParamName = "Counterpart",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"ClientInstStatus",new IncomingParamDescriptor() { ParamName = "ClientInstStatus",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"NostroSecAcc",new IncomingParamDescriptor() { ParamName = "NostroSecAcc",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"TradeDate",new IncomingParamDescriptor() { ParamName = "TradeDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"TradeDateEnd",new IncomingParamDescriptor() { ParamName = "TradeDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"SettDate",new IncomingParamDescriptor() { ParamName = "SettDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"SettDateEnd",new IncomingParamDescriptor() { ParamName = "SettDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"Taxable",new IncomingParamDescriptor() { ParamName = "Taxable",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"JSEStatus",new IncomingParamDescriptor() { ParamName = "JSEStatus",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            ////{"SubStatus",new IncomingParamDescriptor() { ParamName = "SubStatus",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            ////{"Market",new IncomingParamDescriptor() { ParamName = "Market",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"PaymentReference",new IncomingParamDescriptor() { ParamName = "PaymentReference",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"CreationDate",new IncomingParamDescriptor() { ParamName = "CreationDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"CreationDateEnd",new IncomingParamDescriptor() { ParamName = "CreationDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"UpdateDate",new IncomingParamDescriptor() { ParamName = "UpdateDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"UpdateDateEnd",new IncomingParamDescriptor() { ParamName = "UpdateDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"AssetFamily",new IncomingParamDescriptor() { ParamName = "AssetFamily",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"CreatorUserID",new IncomingParamDescriptor() { ParamName = "CreatorUserID",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"UpdateUserID",new IncomingParamDescriptor() { ParamName = "UpdateUserID",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            ////{"OrderQuantity",new IncomingParamDescriptor() { ParamName = "OrderQuantity",DbType = OracleDbType.Int32,IncomingType = typeof(Nullable<Int32>),FilterType = QueryFilterTypes.Standard}},
        };

        public readonly static Dictionary<string, IncomingParamDescriptor> ListClientInstructionsMoneyMarket = new Dictionary<string, IncomingParamDescriptor>()
        {

            ////{"MainReference",new IncomingParamDescriptor(){ParamName="MainReference",DbType=OracleDbType.Varchar2,IncomingType=Type.GetType("System.String"),FilterType=QueryFilterTypes.Standard}},
            ////{"Client",new IncomingParamDescriptor() { ParamName = "Client",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"FI",new IncomingParamDescriptor() { ParamName = "FI",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"Counterpart",new IncomingParamDescriptor() { ParamName = "Counterpart",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"TradeDate",new IncomingParamDescriptor() { ParamName = "TradeDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"TradeDateEnd",new IncomingParamDescriptor() { ParamName = "TradeDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"ClientRef",new IncomingParamDescriptor(){ParamName="ClientRef",DbType=OracleDbType.Varchar2,IncomingType=Type.GetType("System.String"),FilterType=QueryFilterTypes.Standard}},
            {"SecAcc",new IncomingParamDescriptor() { ParamName = "SecAcc",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"Quantity",new IncomingParamDescriptor() { ParamName = "Quantity",DbType = OracleDbType.Int32,IncomingType = typeof(Nullable<Int32>),FilterType = QueryFilterTypes.Standard}},
            {"ClientInstStatus",new IncomingParamDescriptor() { ParamName = "ClientInstStatus",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"SettDate",new IncomingParamDescriptor() { ParamName = "SettDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"SettDateEnd",new IncomingParamDescriptor() { ParamName = "SettDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"AppRef",new IncomingParamDescriptor() { ParamName = "AppRef",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"TransType",new IncomingParamDescriptor() { ParamName = "TransType",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"NostroSecAcc",new IncomingParamDescriptor() { ParamName = "NostroSecAcc",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"CreationDate",new IncomingParamDescriptor() { ParamName = "CreationDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"CreationDateEnd",new IncomingParamDescriptor() { ParamName = "CreationDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"CreatorUserID",new IncomingParamDescriptor() { ParamName = "CreatorUserID",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"UpdateDate",new IncomingParamDescriptor() { ParamName = "UpdateDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"UpdateDateEnd",new IncomingParamDescriptor() { ParamName = "UpdateDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"UpdateUserID",new IncomingParamDescriptor() { ParamName = "UpdateUserID",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            ////{"Custodian",new IncomingParamDescriptor() { ParamName = "Custodian",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
        };

        public readonly static Dictionary<string, IncomingParamDescriptor> ListClientInstructionsBonds = new Dictionary<string, IncomingParamDescriptor>()
        {
            {"OrderID",new IncomingParamDescriptor(){ParamName="OrderID",DbType=OracleDbType.Varchar2,IncomingType=Type.GetType("System.String"),FilterType=QueryFilterTypes.Standard}},
            {"ClientRef",new IncomingParamDescriptor(){ParamName="ClientRef",DbType=OracleDbType.Varchar2,IncomingType=Type.GetType("System.String"),FilterType=QueryFilterTypes.Standard}},
            {"AppRef",new IncomingParamDescriptor() { ParamName = "AppRef",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            ////{"Client",new IncomingParamDescriptor() { ParamName = "Client",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"SecAcc",new IncomingParamDescriptor() { ParamName = "SecAcc",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"TransType",new IncomingParamDescriptor() { ParamName = "TransType",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"FI",new IncomingParamDescriptor() { ParamName = "FI",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"Quantity",new IncomingParamDescriptor() { ParamName = "Quantity",DbType = OracleDbType.Int32,IncomingType = typeof(Nullable<Int32>),FilterType = QueryFilterTypes.Standard}},
            {"OffEx",new IncomingParamDescriptor() { ParamName = "OffEx",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"Cpty",new IncomingParamDescriptor() { ParamName = "Cpty",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"ClientInstructionStatus",new IncomingParamDescriptor() { ParamName = "ClientInstructionStatus",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"NostroSecAcc",new IncomingParamDescriptor() { ParamName = "NostroSecAcc",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"TradeDate",new IncomingParamDescriptor() { ParamName = "TradeDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"TradeDateEnd",new IncomingParamDescriptor() { ParamName = "TradeDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"SettDate",new IncomingParamDescriptor() { ParamName = "SettDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"SettDateEnd",new IncomingParamDescriptor() { ParamName = "SettDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"Taxable",new IncomingParamDescriptor() { ParamName = "Taxable",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"PaymentReference",new IncomingParamDescriptor() { ParamName = "PaymentReference",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"CreationDate",new IncomingParamDescriptor() { ParamName = "CreationDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"CreationDateEnd",new IncomingParamDescriptor() { ParamName = "CreationDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"UpdateDate",new IncomingParamDescriptor() { ParamName = "UpdateDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"UpdateDateEnd",new IncomingParamDescriptor() { ParamName = "UpdateDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"CreatorUserID",new IncomingParamDescriptor() { ParamName = "CreatorUserID",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"UpdateUserID",new IncomingParamDescriptor() { ParamName = "UpdateUserID",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            ////{"Custodian",new IncomingParamDescriptor() { ParamName = "Custodian",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}}
        };        

        public readonly static Dictionary<string, IncomingParamDescriptor> ListClientInstructionsClientPosition = new Dictionary<string, IncomingParamDescriptor>()
        {
            //{"Client",new IncomingbParamDescriptorBase() { ParamName = "Client",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"PositionDate",new IncomingParamDescriptor() { ParamName = "PositionDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"PositionDateEnd",new IncomingParamDescriptor() { ParamName = "PositionDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"ValidityDate",new IncomingParamDescriptor() { ParamName = "ValidityDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            //{"Origin",new IncomingbParamDescriptorBase(){ParamName="Origin",DbType=OracleDbType.Varchar2,IncomingType=Type.GetType("System.String"),FilterType=QueryFilterTypes.Standard}},
            {"SecAcc",new IncomingParamDescriptor() { ParamName = "SecAcc",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"Impacted",new IncomingParamDescriptor() { ParamName = "Impacted",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"TradableAsset",new IncomingParamDescriptor() { ParamName = "TradableAsset",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"Depo",new IncomingParamDescriptor() { ParamName = "Depo",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"Quantity",new IncomingParamDescriptor() { ParamName = "Quantity",DbType = OracleDbType.Int32,IncomingType = typeof(Nullable<Int32>),FilterType = QueryFilterTypes.Standard}},
            {"Processed",new IncomingParamDescriptor() { ParamName = "Processed",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            //{"CounterpartCode",new IncomingbParamDescriptorBase() { ParamName = "CounterpartCode",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"PositionNature",new IncomingParamDescriptor() { ParamName = "PositionNature",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            //{"Description",new IncomingbParamDescriptorBase() { ParamName = "Description",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            //{"Proxy",new IncomingbParamDescriptorBase() { ParamName = "Proxy",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            //{"AssetNature",new IncomingbParamDescriptorBase() { ParamName = "AssetNature",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"MainReference",new IncomingParamDescriptor() { ParamName = "MainReference",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"PositionIdentifier",new IncomingParamDescriptor() { ParamName = "PositionIdentifier",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"CreationDate",new IncomingParamDescriptor() { ParamName = "CreationDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"CreationDateEnd",new IncomingParamDescriptor() { ParamName = "CreationDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"UpdateDate",new IncomingParamDescriptor() { ParamName = "UpdateDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"UpdateDateEnd",new IncomingParamDescriptor() { ParamName = "UpdateDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}}
        };

        public readonly static Dictionary<string, IncomingParamDescriptor> ListClientInstructionsListOperationsDematRemat = new Dictionary<string, IncomingParamDescriptor>()
        {
            {"OrderID",new IncomingParamDescriptor(){ParamName="OrderID",DbType=OracleDbType.Varchar2,IncomingType=Type.GetType("System.String"),FilterType=QueryFilterTypes.Standard}},
            {"ClientRef",new IncomingParamDescriptor(){ParamName="ClientRef",DbType=OracleDbType.Varchar2,IncomingType=Type.GetType("System.String"),FilterType=QueryFilterTypes.Standard}},
            {"AppRef",new IncomingParamDescriptor() { ParamName = "AppRef",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            //{"Client",new IncomingParamDescriptor() { ParamName = "Client",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"SecAcc",new IncomingParamDescriptor() { ParamName = "SecAcc",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"TransType",new IncomingParamDescriptor() { ParamName = "TransType",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"FI",new IncomingParamDescriptor() { ParamName = "FI",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"Quantity",new IncomingParamDescriptor() { ParamName = "Quantity",DbType = OracleDbType.Int32,IncomingType = typeof(Nullable<Int32>),FilterType = QueryFilterTypes.Standard}},
            {"OffEx",new IncomingParamDescriptor() { ParamName = "OffEx",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"PhysicalLegStatus",new IncomingParamDescriptor() { ParamName = "PhysicalLegStatus",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"ElectronicLegStatus",new IncomingParamDescriptor() { ParamName = "ElectronicLegStatus",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"NostroSecAcc",new IncomingParamDescriptor() { ParamName = "NostroSecAcc",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"TradeDate",new IncomingParamDescriptor() { ParamName = "TradeDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"TradeDateEnd",new IncomingParamDescriptor() { ParamName = "TradeDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"SettDate",new IncomingParamDescriptor() { ParamName = "SettDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"SettDateEnd",new IncomingParamDescriptor() { ParamName = "SettDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"Taxable",new IncomingParamDescriptor() { ParamName = "Taxable",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"CreationDate",new IncomingParamDescriptor() { ParamName = "CreationDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"CreationDateEnd",new IncomingParamDescriptor() { ParamName = "CreationDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"UpdateDate",new IncomingParamDescriptor() { ParamName = "UpdateDate",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeStart}},
            {"UpdateDateEnd",new IncomingParamDescriptor() { ParamName = "UpdateDateEnd",DbType = OracleDbType.Date,IncomingType = typeof(Nullable<System.DateTime>),FilterType = QueryFilterTypes.DateRangeEnd}},
            {"CreatorUserID",new IncomingParamDescriptor() { ParamName = "CreatorUserID",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            {"UpdateUserID",new IncomingParamDescriptor() { ParamName = "UpdateUserID",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            //{"AssetFamily",new IncomingParamDescriptor() { ParamName = "AssetFamily",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}},
            //{"Custodian",new IncomingParamDescriptor() { ParamName = "Custodian",DbType = OracleDbType.Varchar2,IncomingType = Type.GetType("System.String"),FilterType = QueryFilterTypes.Standard}}
        };
    }
}
