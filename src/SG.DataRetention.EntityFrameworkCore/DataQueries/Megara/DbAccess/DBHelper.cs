﻿using Oracle.ManagedDataAccess.Client;
using SG.DataRetention.DataQueries.Common;
using SG.DataRetention.DataQueries.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SG.DataRetention.DataQueries.Megara.DbAccess
{
    public static class DBHelper<T>
    {
        private static readonly Dictionary<QueryFilterTypes, String> TypeToOperatorMapper = new Dictionary<QueryFilterTypes, string>() {
            {QueryFilterTypes.DateRangeStart, " >= " },
            {QueryFilterTypes.DateRangeEnd, " <= " },
            {QueryFilterTypes.Standard, " = " }
        };

        private static readonly Dictionary<QueryFilterTypes, Delegate> TypeToSanitizeMapper = new Dictionary<QueryFilterTypes, Delegate>() {
            {QueryFilterTypes.DateRangeStart, new Func<object, object>(SanitizeDateStart) },
            {QueryFilterTypes.DateRangeEnd, new Func<object, object>(SanitizeDateEnd) },
            {QueryFilterTypes.Standard, new Func<object, object>(SanitizeInput) }
        };

        private static bool IsNumeric(string value)
        {
            return value.All(char.IsNumber);
        }

        private static object SanitizeInput(object value)
        {
            if(value == null)
            {
                return value;
            }
            if (value.GetType() == typeof(string))
            {
                value = value.ToString().Trim();
                return String.IsNullOrEmpty(value.ToString()) ? null : value;
            }
            if (IsNumeric(value.ToString())){
                if (value.Equals(0))
                {
                    return null;
                }
                return value;
            }
            return value;
        }

        private static object SanitizeDateEnd(object value)
        {
            if (value == null)
            {
                return value;
            }
            value = ((DateTime)value).Date.AddDays(1);
            return value;
        }

        private static object SanitizeDateStart(object value)
        {
            if (value == null)
            {
                return value;
            }
            value = ((DateTime)value).Date;
            return value;
        }

        public static OracleCommand BuildCommandDynamicFilter(T input, DataPagingInput paging, string baseQuery, Dictionary<string, IncomingParamDescriptor> possibleParameters)
        {
            OracleCommand cmd = new OracleCommand();
            string dynamicWhere = String.Empty;
            string andOperator = " and ";
            foreach (PropertyInfo prop in input.GetType().GetProperties())
            {
                if (!(possibleParameters.ContainsKey(prop.Name)))
                {
                    continue;
                }
                var paramDescriptor = possibleParameters[prop.Name];
                if (!(paramDescriptor.IncomingType == prop.PropertyType))
                {
                    throw new Exception("Parameter Type mistmatch");
                }

                //var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                var propValue = prop.GetValue(input);
                var param = GetOracleParam(propValue, paramDescriptor);
                if(param == null)
                {
                    continue;
                }
                cmd.Parameters.Add(param);
                string logicOperator = TypeToOperatorMapper.GetValueOrDefault(paramDescriptor.FilterType);              
                dynamicWhere += paramDescriptor.ParamName + logicOperator + ":" + paramDescriptor.ParamName + andOperator;
            }

            if (!(dynamicWhere == String.Empty))
            {
                dynamicWhere = System.Environment.NewLine + "WHERE " + dynamicWhere.Substring(0, dynamicWhere.Length - andOperator.Length);

            }
            string offsetParamName = "OffsetParam";
            string pageNumberParamName = "PageParam";
            string orderBy = Environment.NewLine + "order by 1";
            string dynamicPaging = $"{Environment.NewLine}OFFSET :{offsetParamName} ROWS" +
                $"{Environment.NewLine}FETCH NEXT :{pageNumberParamName} ROWS ONLY";

            int defaultPageSize = 50;
            int pageNumber = paging.PageNumber < 1 ? 1 : paging.PageNumber;
            int pageSize = paging.PageSize < 1 ? defaultPageSize : paging.PageSize;

            int offsetRecords = (pageNumber - 1) * pageSize;

            var paramOffset = new OracleParameter(offsetParamName, OracleDbType.Int16)
            {
                Value = offsetRecords,
                Direction = System.Data.ParameterDirection.Input
            };
            cmd.Parameters.Add(paramOffset);
            var paramPageNumber = new OracleParameter(pageNumberParamName, OracleDbType.Int16)
            {
                Value = pageSize,
                Direction = System.Data.ParameterDirection.Input
            };
            cmd.Parameters.Add(paramPageNumber);

            string fullQuery = baseQuery + dynamicWhere + orderBy + dynamicPaging;
            cmd.CommandText = fullQuery;
            cmd.CommandType = System.Data.CommandType.Text;

            return cmd;
        }

        private static OracleParameter GetOracleParam(Object propValue, IncomingParamDescriptor paramDescriptor)
        {
            propValue = TypeToSanitizeMapper[paramDescriptor.FilterType].DynamicInvoke(propValue);
            if (propValue == null)
            {
                return null;
            }
            var param = new OracleParameter(paramDescriptor.ParamName, paramDescriptor.DbType)
            {
                Value = propValue,
                Direction = System.Data.ParameterDirection.Input
            };
            return param;
        }

        public static OracleCommand BuildCountCommandDynamicFilter(T input, string baseQuery, Dictionary<string, IncomingParamDescriptor> possibleParameters)
        {
            OracleCommand cmd = new OracleCommand();
            string dynamicWhere = String.Empty;
            string andOperator = " and ";
            //string compareOperator = " = "; //work on this 
            try 
            { 
            foreach (PropertyInfo prop in input.GetType().GetProperties())
            {
                if (!(possibleParameters.ContainsKey(prop.Name)))
                {
                    continue;
                }
                var paramDescriptor = possibleParameters[prop.Name];
                if (!(paramDescriptor.IncomingType == prop.PropertyType))
                {
                    throw new Exception("Parameter Type mistmatch");
                }
                //var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                var propValue = prop.GetValue(input);
                var param = GetOracleParam(propValue, paramDescriptor);
                if (param == null)
                {
                    continue;
                }
                cmd.Parameters.Add(param);
                string logicOperator = TypeToOperatorMapper.GetValueOrDefault(paramDescriptor.FilterType);
                dynamicWhere += paramDescriptor.ParamName + logicOperator + ":" + paramDescriptor.ParamName + andOperator;

            }
            }
            catch(System.Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
            }
            if (!(dynamicWhere == String.Empty))
            {
                dynamicWhere = System.Environment.NewLine + "WHERE " + dynamicWhere.Substring(0, dynamicWhere.Length - andOperator.Length);

            }
            //dynamicWhere += ";";
            string fullQuery = baseQuery + dynamicWhere;
            string cmdText = $"SELECT COUNT(*) from " +
                            $"{Environment.NewLine}(" +
                            $"{fullQuery}" +
                            $"{Environment.NewLine})";
            cmd.CommandText = cmdText;
            cmd.CommandType = System.Data.CommandType.Text;

            return cmd;
        }
    }
}
