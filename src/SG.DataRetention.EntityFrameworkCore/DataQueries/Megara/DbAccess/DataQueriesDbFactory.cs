﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using SG.DataRetention.Configuration;
using SG.DataRetention.Web;
using System;

namespace SG.DataRetention.DataQueries.Megara
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class DataQueriesDbFactory 
    {
        public static IMegaraDataAccess CreateMegaraDataAccess(bool isMock)
        {
            if (isMock)
            {
                return CreateMegaraDataAccessMock();
            }
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder(), addUserSecrets: true);
            var megaraConnectionString = configuration.GetConnectionString(DataRetentionConsts.MegaraConnectionStringName);
            return new MegaraDataAccess(megaraConnectionString);
        }

        private static IMegaraDataAccess CreateMegaraDataAccessMock()
        {
            return new MegaraDataAccessMock();
        }
        
    }
}