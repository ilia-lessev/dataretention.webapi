﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SG.DataRetention.Configuration;
using SG.DataRetention.DataQueries.Common.Dto;
using SG.DataRetention.DataQueries.Megara.Dto;
using SG.DataRetention.Web;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using Abp.Dependency;

namespace SG.DataRetention.DataQueries.Megara
{
    public class MegaraRepository : IMegaraRepository, ITransientDependency
    {
        private bool _isMock = true;
        private IMegaraDataAccess _megaraDbAccess = null;
        public MegaraRepository()
        {
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder(), addUserSecrets: true);
            _isMock = Convert.ToBoolean(configuration["DataQueries:UseMock"]);
            _megaraDbAccess = DataQueriesDbFactory.CreateMegaraDataAccess(_isMock);
        }
        public async Task<List<ClientInstructionsEquityOutput>> GetClientInstructionsEquityAsync(ClientInstructionsEquityParams input, DataPagingInput paging)
        {
            var dataQueryResults = await _megaraDbAccess.GetClientInstructionsEquityAsync(input, paging);
            return dataQueryResults; 
        }

        public async Task<long> CountClientInstructionsEquityAsync(ClientInstructionsEquityParams input)
        {
            var countRecords = await _megaraDbAccess.CountClientInstructionsEquityAsync(input);
            return countRecords;
        }

        public async Task<List<ClientInstructionsBondsOutput>> GetClientInstructionsBondsAsync(ClientInstructionsBondsParams input, DataPagingInput paging)
        {
            var dataQueryResults = await _megaraDbAccess.GetClientInstructionsBondsAsync(input, paging);
            return dataQueryResults;
        }

        public async Task<long> CountClientInstructionsBondsAsync(ClientInstructionsBondsParams input)
        {
            var countRecords = await _megaraDbAccess.CountClientInstructionsBondsAsync(input);
            return countRecords;
        }

        public async Task<List<ClientInstructionsMoneyMarketOutput>> GetClientInstructionsMoneyMarketAsync(ClientInstructionsMoneyMarketParams input, DataPagingInput paging)
        {
            var dataQueryResults = await _megaraDbAccess.GetClientInstructionsMoneyMarketAsync(input, paging);
            return dataQueryResults;
        }

        public async Task<long> CountClientInstructionsMoneyMarketAsync(ClientInstructionsMoneyMarketParams input)
        {
            var countRecords = await _megaraDbAccess.CountClientInstructionsMoneyMarketAsync(input);
            return countRecords;
        }

        public async Task<List<ClientPositionOutput>> GetClientPositionAsync(ClientPositionParams input, DataPagingInput paging)
        {
            DateTime? validityDate = null;
            bool setValidity = false;
            if(input.PositionDateEnd!=null)
            {
                validityDate = input.PositionDateEnd;
                setValidity = true;
            }
            if (input.PositionDate != null)
            {
                validityDate = input.PositionDate;
                setValidity = true;
            }
            if(setValidity)
            {
                input.ValidityDate = validityDate;
            }
            var dataQueryResults = await _megaraDbAccess.GetClientPositionAsync(input, paging);
            return dataQueryResults;
        }

        public async Task<long> CountClientPositionAsync(ClientPositionParams input)
        {
            var countRecords = await _megaraDbAccess.CountClientPositionAsync(input);
            return countRecords;
        }

        public async Task<List<DematRematOperationsOutput>> DematRematOperationsAsync(DematRematOperationsParams input, DataPagingInput paging)
        {
            var dataQueryResults = await _megaraDbAccess.DematRematOperationsAsync(input, paging);
            return dataQueryResults;
        }

        public async Task<long> CountDematRematOperationsAsync(DematRematOperationsParams input)
        {
            var countRecords = await _megaraDbAccess.CountDematRematOperationsAsync(input);
            return countRecords;
        }
    }
}
