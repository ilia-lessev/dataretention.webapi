﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc;
using SG.DataRetention.DataQueries.Common.Dto;
using SG.DataRetention.DataQueries.Megara.Dto;

namespace SG.DataRetention.DataQueries.Megara
{
    public interface IMegaraRepository
    {
        Task<List<ClientInstructionsEquityOutput>> GetClientInstructionsEquityAsync(ClientInstructionsEquityParams input, DataPagingInput paging);
        Task<long> CountClientInstructionsEquityAsync(ClientInstructionsEquityParams input);
        Task<List<ClientInstructionsBondsOutput>> GetClientInstructionsBondsAsync(ClientInstructionsBondsParams input, DataPagingInput paging);
        Task<long> CountClientInstructionsBondsAsync(ClientInstructionsBondsParams input);
        Task<List<ClientInstructionsMoneyMarketOutput>> GetClientInstructionsMoneyMarketAsync(ClientInstructionsMoneyMarketParams input, DataPagingInput paging);
        Task<long> CountClientInstructionsMoneyMarketAsync(ClientInstructionsMoneyMarketParams input);
        Task<List<ClientPositionOutput>> GetClientPositionAsync(ClientPositionParams input, DataPagingInput paging);
        Task<long> CountClientPositionAsync(ClientPositionParams input);
        Task<List<DematRematOperationsOutput>> DematRematOperationsAsync(DematRematOperationsParams input, DataPagingInput paging);
        Task<long> CountDematRematOperationsAsync(DematRematOperationsParams input);
    }
}
