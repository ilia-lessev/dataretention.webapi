﻿using Abp.Authorization;
using SG.DataRetention.Authorization.Roles;
using SG.DataRetention.Authorization.Users;

namespace SG.DataRetention.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
