﻿using System.Threading.Tasks;
using Abp.Domain.Policies;

namespace SG.DataRetention.Authorization.Users
{
    public interface IUserPolicy : IPolicy
    {
        Task CheckMaxUserCountAsync(int tenantId);
    }
}
