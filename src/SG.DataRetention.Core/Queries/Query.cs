﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;

namespace SG.DataRetention.Queries
{
    [Table("Queries")]
    public class Query : FullAuditedEntity
    {
        public const int MaxPasswordLength = 32;

        [Required]
        public int QueryId { get; set; }
        [Display(Name = "Query Reference")]
        public string QueryReference { get; set; }
        [Display(Name = "Assigned To")]
        public string AssignedTo { get; set; }
        public string Department { get; set; }
        [Display(Name = "Query Type")]
        public string QueryType { get; set; }
        [Display(Name = "Query Description")]
        public string QueryDescription { get; set; }
        [Display(Name = "Query Status")]
        public string QueryStatus { get; set; }
        public string SLA { get; set; }
        [Display(Name = "Delete Reason")]
        public string DeleteReason { get; set; }
        public bool FinancialInstrument_Equities { get; set; }
        public bool FinancialInstrument_Bonds { get; set; }
        public bool FinancialInstrument_MoneyMarket { get; set; }
        public bool FinancialInstrument_Derivatives { get; set; }
        public string CompanyRegistrationNumber { get; set; }
        public string CompanyName { get; set; }
        public int EntityNumber { get; set; }
        public int CashAccountNumber { get; set; }
        public int SecurityAccountNumber { get; set; }
        public string AuthorisedContactPerson { get; set; }
        public string Email { get; set; }
        public int ContactNumber { get; set; }  
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        [NotMapped]
        public List<string> AttachmentNames { get; set; }

    }
}
