﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SG.DataRetention.Queries
{
    [Table("QueryType")]
    public class QueryType : Entity
    {
        [Column("TypeId")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override int Id { get; set; }
        public string TypeName { get; set; }
    }
}
