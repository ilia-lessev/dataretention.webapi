﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SG.DataRetention.Queries
{
    [Table("DepartmentQueryType")]
    public class DepartmentQueryType:Entity
    {
        [ForeignKey("Department")]
        public int DepartmentId { get; set; } 
        public Department Department { get; set; }

        [ForeignKey("QueryType")]
        public int TypeId { get; set; }       
        public QueryType QueryType { get; set; }
    }
}
