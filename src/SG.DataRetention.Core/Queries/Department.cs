﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SG.DataRetention.Queries
{
    [Table("Departments")]
    public class Department : Entity
    {  
        [Column("DepartmentId")]    
        [Key]   
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string DepartmentName { get; set; }           
      
    }
}
