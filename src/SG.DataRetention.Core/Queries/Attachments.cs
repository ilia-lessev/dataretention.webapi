﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;

namespace SG.DataRetention.Queries
{
    [Table("Attachments")]
    public class Attachments : Entity
    {
        [Column("AttachmentId")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override int Id { get; set; }

        public string AttachmentName { get; set; }

        [ForeignKey("Query")]
        public int QueryId { get; set; }
        public Query Query { get; set; }

    }
}
