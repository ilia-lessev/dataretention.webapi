﻿using System.Collections.Generic;

namespace SG.DataRetention.DashboardCustomization
{
    public class Dashboard
    {
        public string DashboardName { get; set; }

        public List<Page> Pages { get; set; }
    }
}
