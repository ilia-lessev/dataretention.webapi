﻿using Abp.Events.Bus;

namespace SG.DataRetention.MultiTenancy
{
    public class RecurringPaymentsEnabledEventData : EventData
    {
        public int TenantId { get; set; }
    }
}