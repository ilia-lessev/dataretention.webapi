﻿using System.Threading.Tasks;
using Abp.Dependency;

namespace SG.DataRetention.MultiTenancy.Accounting
{
    public interface IInvoiceNumberGenerator : ITransientDependency
    {
        Task<string> GetNewInvoiceNumber();
    }
}