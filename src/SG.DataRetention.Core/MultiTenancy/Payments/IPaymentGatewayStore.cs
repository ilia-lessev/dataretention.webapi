﻿using System.Collections.Generic;

namespace SG.DataRetention.MultiTenancy.Payments
{
    public interface IPaymentGatewayStore
    {
        List<PaymentGatewayModel> GetActiveGateways();
    }
}
