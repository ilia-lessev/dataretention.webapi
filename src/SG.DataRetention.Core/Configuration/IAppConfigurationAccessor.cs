﻿using Microsoft.Extensions.Configuration;

namespace SG.DataRetention.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}
