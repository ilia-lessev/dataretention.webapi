﻿using Abp.Domain.Services;

namespace SG.DataRetention
{
    public abstract class DataRetentionDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected DataRetentionDomainServiceBase()
        {
            LocalizationSourceName = DataRetentionConsts.LocalizationSourceName;
        }
    }
}
