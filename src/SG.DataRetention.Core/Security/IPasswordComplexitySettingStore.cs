﻿using System.Threading.Tasks;

namespace SG.DataRetention.Security
{
    public interface IPasswordComplexitySettingStore
    {
        Task<PasswordComplexitySetting> GetSettingsAsync();
    }
}
