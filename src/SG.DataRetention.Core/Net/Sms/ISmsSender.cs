using System.Threading.Tasks;

namespace SG.DataRetention.Net.Sms
{
    public interface ISmsSender
    {
        Task SendAsync(string number, string message);
    }
}