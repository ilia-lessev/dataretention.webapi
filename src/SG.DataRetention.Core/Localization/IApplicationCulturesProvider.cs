﻿using System.Globalization;

namespace SG.DataRetention.Localization
{
    public interface IApplicationCulturesProvider
    {
        CultureInfo[] GetAllCultures();
    }
}