﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
namespace SG.DataRetention.Password
{
    [Table("PasswordHistory")]
    public class PasswordHistory : FullAuditedEntity
    {
        public const int MaxPasswordLength = 32;

        [Required]
        public int UserId { get; set; }

        [Required]
        [MaxLength(MaxPasswordLength)]
        public virtual string Password { get; set; }

        public System.DateTime DateChanged { get; set; }
    }
}
