﻿using System.Threading.Tasks;
using SG.DataRetention.Views;
using Xamarin.Forms;

namespace SG.DataRetention.Services.Modal
{
    public interface IModalService
    {
        Task ShowModalAsync(Page page);

        Task ShowModalAsync<TView>(object navigationParameter) where TView : IXamarinView;

        Task<Page> CloseModalAsync();
    }
}
