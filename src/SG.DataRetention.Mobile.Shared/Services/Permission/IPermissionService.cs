﻿namespace SG.DataRetention.Services.Permission
{
    public interface IPermissionService
    {
        bool HasPermission(string key);
    }
}