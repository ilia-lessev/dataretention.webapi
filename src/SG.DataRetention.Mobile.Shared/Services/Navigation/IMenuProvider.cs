using System.Collections.Generic;
using MvvmHelpers;
using SG.DataRetention.Models.NavigationMenu;

namespace SG.DataRetention.Services.Navigation
{
    public interface IMenuProvider
    {
        ObservableRangeCollection<NavigationMenuItem> GetAuthorizedMenuItems(Dictionary<string, string> grantedPermissions);
    }
}