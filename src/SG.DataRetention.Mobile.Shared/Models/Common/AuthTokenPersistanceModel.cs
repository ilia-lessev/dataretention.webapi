﻿using System;
using Abp.AutoMapper;
using SG.DataRetention.Sessions.Dto;

namespace SG.DataRetention.Models.Common
{
    [AutoMapFrom(typeof(ApplicationInfoDto)),
     AutoMapTo(typeof(ApplicationInfoDto))]
    public class ApplicationInfoPersistanceModel
    {
        public string Version { get; set; }

        public DateTime ReleaseDate { get; set; }
    }
}