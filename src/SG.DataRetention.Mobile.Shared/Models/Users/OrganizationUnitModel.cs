﻿using Abp.AutoMapper;
using SG.DataRetention.Organizations.Dto;

namespace SG.DataRetention.Models.Users
{
    [AutoMapFrom(typeof(OrganizationUnitDto))]
    public class OrganizationUnitModel : OrganizationUnitDto
    {
        public bool IsAssigned { get; set; }
    }
}