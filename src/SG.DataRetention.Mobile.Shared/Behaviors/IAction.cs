﻿using Xamarin.Forms.Internals;

namespace SG.DataRetention.Behaviors
{
    [Preserve(AllMembers = true)]
    public interface IAction
    {
        bool Execute(object sender, object parameter);
    }
}