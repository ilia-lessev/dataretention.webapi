﻿using System;
using SG.DataRetention.Core;
using SG.DataRetention.Core.Dependency;
using SG.DataRetention.Services.Permission;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SG.DataRetention.Extensions.MarkupExtensions
{
    [ContentProperty("Text")]
    public class HasPermissionExtension : IMarkupExtension
    {
        public string Text { get; set; }
        
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (ApplicationBootstrapper.AbpBootstrapper == null || Text == null)
            {
                return false;
            }

            var permissionService = DependencyResolver.Resolve<IPermissionService>();
            return permissionService.HasPermission(Text);
        }
    }
}