﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace SG.DataRetention
{
    [DependsOn(typeof(DataRetentionClientModule), typeof(AbpAutoMapperModule))]
    public class DataRetentionXamarinSharedModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Localization.IsEnabled = false;
            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DataRetentionXamarinSharedModule).GetAssembly());
        }
    }
}