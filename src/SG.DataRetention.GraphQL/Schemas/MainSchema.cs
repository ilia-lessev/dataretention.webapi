﻿using Abp.Dependency;
using GraphQL;
using GraphQL.Types;
using SG.DataRetention.Queries.Container;

namespace SG.DataRetention.Schemas
{
    public class MainSchema : Schema, ITransientDependency
    {
        public MainSchema(IDependencyResolver resolver) :
            base(resolver)
        {
            Query = resolver.Resolve<QueryContainer>();
        }
    }
}