﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Security;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Zero.Configuration;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SG.DataRetention.Authorization.Accounts.Dto;
using SG.DataRetention.Authorization.Impersonation;
using SG.DataRetention.Authorization.Users;
using SG.DataRetention.Configuration;
using SG.DataRetention.DataQueries.Common.Dto;
using SG.DataRetention.DataQueries.Megara;
using SG.DataRetention.DataQueries.Megara.Dto;
using SG.DataRetention.Debugging;
using SG.DataRetention.EntityFrameworkCore;
using SG.DataRetention.MultiTenancy;
using SG.DataRetention.MultiTenancy.Payments;
using SG.DataRetention.Password;
using SG.DataRetention.Security.Recaptcha;
using SG.DataRetention.Url;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace SG.DataRetention.Authorization.Accounts
{
    public class MegaraAppService : DataRetentionAppServiceBase, IMegaraAppService
    {
        private readonly IMegaraRepository _megaraRepository;

        public MegaraAppService(IMegaraRepository megaraRepository)
        {
            _megaraRepository = megaraRepository;
        }

        public async Task<ListResultDto<ClientInstructionsEquityOutput>> ClientInstructionsEquity(ClientInstructionsEquityInput input)
        {
            var queryResults = await _megaraRepository.GetClientInstructionsEquityAsync(input.QueryParams, input.Paging);
            var convertedResult = new ListResultDto<ClientInstructionsEquityOutput>(ObjectMapper.Map<List<ClientInstructionsEquityOutput>>(queryResults));
            return convertedResult;
        }

        public async Task<long> CountClientInstructionsEquity(ClientInstructionsEquityParams input)
        {
            var recordsCount = await _megaraRepository.CountClientInstructionsEquityAsync(input);
            return recordsCount;
        }

        public async Task<ListResultDto<ClientInstructionsBondsOutput>> ClientInstructionsBonds(ClientInstructionsBondsInput input)
        {
            var queryResults = await _megaraRepository.GetClientInstructionsBondsAsync(input.QueryParams, input.Paging);
            var convertedResult = new ListResultDto<ClientInstructionsBondsOutput>(ObjectMapper.Map<List<ClientInstructionsBondsOutput>>(queryResults));
            return convertedResult;
        }

        public async Task<long> CountClientInstructionsBonds(ClientInstructionsBondsParams input)
        {
            var recordsCount = await _megaraRepository.CountClientInstructionsBondsAsync(input);
            return recordsCount;
        }

        public async Task<ListResultDto<ClientInstructionsMoneyMarketOutput>> ClientInstructionsMoneyMarket(ClientInstructionsMoneyMarketInput input)
        {
            var queryResults = await _megaraRepository.GetClientInstructionsMoneyMarketAsync(input.QueryParams, input.Paging);
            var convertedResult = new ListResultDto<ClientInstructionsMoneyMarketOutput>(ObjectMapper.Map<List<ClientInstructionsMoneyMarketOutput>>(queryResults));
            return convertedResult;
        }

        public async Task<long> CountClientInstructionsMoneyMarket(ClientInstructionsMoneyMarketParams input)
        {
            var recordsCount = await _megaraRepository.CountClientInstructionsMoneyMarketAsync(input);
            return recordsCount;
        }

        public async Task<ListResultDto<ClientPositionOutput>> ClientPosition(ClientInstructionsClientPositionInput input)
        {
            var queryResults = await _megaraRepository.GetClientPositionAsync(input.QueryParams, input.Paging);
            var convertedResult = new ListResultDto<ClientPositionOutput>(ObjectMapper.Map<List<ClientPositionOutput>>(queryResults));
            return convertedResult;
        }

        public async Task<long> CountClientPosition(ClientPositionParams input)
        {
            var recordsCount = await _megaraRepository.CountClientPositionAsync(input);
            return recordsCount;
        }

        public async Task<ListResultDto<DematRematOperationsOutput>> DematRematOperations(DematRematOperationsInput input)
        {
            var queryResults = await _megaraRepository.DematRematOperationsAsync(input.QueryParams, input.Paging);
            var convertedResult = new ListResultDto<DematRematOperationsOutput>(ObjectMapper.Map<List<DematRematOperationsOutput>>(queryResults));
            return convertedResult;
        }

        public async Task<long> CountDematRematOperations(DematRematOperationsParams input)
        {
            var recordsCount = await _megaraRepository.CountDematRematOperationsAsync(input);
            return recordsCount;
        }
    }
}