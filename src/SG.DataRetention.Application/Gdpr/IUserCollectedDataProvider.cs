﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp;
using SG.DataRetention.Dto;

namespace SG.DataRetention.Gdpr
{
    public interface IUserCollectedDataProvider
    {
        Task<List<FileDto>> GetFiles(UserIdentifier user);
    }
}
