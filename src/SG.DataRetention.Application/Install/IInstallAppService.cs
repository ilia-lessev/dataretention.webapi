﻿using System.Threading.Tasks;
using Abp.Application.Services;
using SG.DataRetention.Install.Dto;

namespace SG.DataRetention.Install
{
    public interface IInstallAppService : IApplicationService
    {
        Task Setup(InstallDto input);

        AppSettingsJsonDto GetAppSettingsJson();

        CheckDatabaseOutput CheckDatabase();
    }
}