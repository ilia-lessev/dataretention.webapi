﻿using System.Collections.Generic;
using SG.DataRetention.Auditing.Dto;
using SG.DataRetention.Dto;

namespace SG.DataRetention.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);

        FileDto ExportToFile(List<EntityChangeListDto> entityChangeListDtos);
    }
}
