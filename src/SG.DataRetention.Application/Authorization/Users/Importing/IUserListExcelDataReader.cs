﻿using System.Collections.Generic;
using SG.DataRetention.Authorization.Users.Importing.Dto;
using Abp.Dependency;

namespace SG.DataRetention.Authorization.Users.Importing
{
    public interface IUserListExcelDataReader: ITransientDependency
    {
        List<ImportUserDto> GetUsersFromExcel(byte[] fileBytes);
    }
}
