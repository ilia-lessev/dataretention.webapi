﻿using System.Collections.Generic;
using SG.DataRetention.Authorization.Users.Importing.Dto;
using SG.DataRetention.Dto;

namespace SG.DataRetention.Authorization.Users.Importing
{
    public interface IInvalidUserExporter
    {
        FileDto ExportToFile(List<ImportUserDto> userListDtos);
    }
}
