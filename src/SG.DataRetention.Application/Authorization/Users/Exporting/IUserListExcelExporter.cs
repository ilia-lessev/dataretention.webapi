using System.Collections.Generic;
using SG.DataRetention.Authorization.Users.Dto;
using SG.DataRetention.Dto;

namespace SG.DataRetention.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}