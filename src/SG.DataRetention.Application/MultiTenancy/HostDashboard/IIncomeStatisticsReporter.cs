﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SG.DataRetention.MultiTenancy.HostDashboard.Dto;

namespace SG.DataRetention.MultiTenancy.HostDashboard
{
    public interface IIncomeStatisticsService
    {
        Task<List<IncomeStastistic>> GetIncomeStatisticsData(DateTime startDate, DateTime endDate,
            ChartDateInterval dateInterval);
    }
}