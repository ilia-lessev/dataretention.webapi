﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SG.DataRetention.MultiTenancy.Accounting.Dto;

namespace SG.DataRetention.MultiTenancy.Accounting
{
    public interface IInvoiceAppService
    {
        Task<InvoiceDto> GetInvoiceInfo(EntityDto<long> input);

        Task CreateInvoice(CreateInvoiceDto input);
    }
}
