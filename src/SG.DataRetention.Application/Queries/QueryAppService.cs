﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.UI;
using SG.DataRetention.Queries.Dto;


namespace SG.DataRetention.Queries
{
    public class QueryAppService : DataRetentionAppServiceBase, IQueryAppService
    {
        private readonly IRepository<Query> _queryRepository;
        private readonly IRepository<Department> _departmentRepository;
        private readonly IRepository<QueryType> _queryTypeRepository;
        private readonly IRepository<DepartmentQueryType> _departmentQueryTypeRepository;
        private readonly IRepository<Attachments> _attachmentRepository;
        private const string Closed = "Closed";

        public QueryAppService(IRepository<Query> queryRepository, IRepository<Department> departmentRepository,
            IRepository<Attachments> attachmentRepository, IRepository<DepartmentQueryType> departmentQueryTypeRepository,
            IRepository<QueryType> queryTypeRepository)
        {
            _queryRepository = queryRepository;
            _departmentRepository = departmentRepository;
            _attachmentRepository = attachmentRepository;
            _departmentQueryTypeRepository = departmentQueryTypeRepository;
            _queryTypeRepository = queryTypeRepository;
        }

        public ListResultDto<QueryListDto> GetQueries()
        {
            var query = _queryRepository
                .GetAll()
                .ToList();

            return new ListResultDto<QueryListDto>(ObjectMapper.Map<List<QueryListDto>>(query));
        }

        public ListResultDto<DepartmentListDto> GetDepartments()
        {
            var departments = _departmentRepository.GetAll().ToList();
            return new ListResultDto<DepartmentListDto>(ObjectMapper.Map<List<DepartmentListDto>>(departments));
        }

        public async Task CreateQuery(QueryCreateDto input)
        {
            try
            {
                var query = ObjectMapper.Map<Query>(input);

                var attachmentList = input.AttachmentNames;

                List<string> attachmentNames = new List<string>();

                if (attachmentList != null)
                {
                    for (int i = 0; i < attachmentList.Count; i++)
                    {
                        attachmentNames.Add(attachmentList[i]);
                    }

                    var attachmentDto = new AttachmentInsertDto
                    {
                        QueryId = input.Id,
                        Query = input,
                        AttachmentName = String.Join(";", attachmentNames.ToArray())
                    };

                    var attachment = ObjectMapper.Map<Attachments>(attachmentDto);
                    await _attachmentRepository.InsertAsync(attachment);
                }

                else
                {
                    await _queryRepository.InsertAsync(query);
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException("Query could not be created.");
            }
        }

        public ListResultDto<QueryTypeListDto> GetQueryTypes(string departmentSelected)
        {
            var departmentId = _departmentRepository.FirstOrDefault(x => x.DepartmentName == departmentSelected).Id;

            var typeIdList = _departmentQueryTypeRepository.GetAll()
                .Where(x => x.DepartmentId == departmentId)
                .Select(x => x.TypeId)
                .ToList();

            var queryTypes = _queryTypeRepository.GetAll()
                .Where(q => typeIdList.Contains(q.Id))
                .ToList();

            return new ListResultDto<QueryTypeListDto>(ObjectMapper.Map<List<QueryTypeListDto>>(queryTypes));
        }

        public async Task CloseQuery(int queryId, string reasonDetails)
        {
            var query = await _queryRepository.GetAsync(Convert.ToInt32(queryId));

            if (!query.QueryStatus.Equals(Closed))
            {
                query.QueryStatus = Closed;
                query.DeleteReason = reasonDetails;
                await _queryRepository.UpdateAsync(query);
            }
            else
            {
                throw new UserFriendlyException(L("QueryAlreadyClosed_Detail"));
            }
        }
    }
}
