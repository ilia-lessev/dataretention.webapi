using System.Threading.Tasks;

namespace SG.DataRetention.Security.Recaptcha
{
    public interface IRecaptchaValidator
    {
        Task ValidateAsync(string captchaResponse);
    }
}