﻿using System.Collections.Generic;
using SG.DataRetention.Chat.Dto;
using SG.DataRetention.Dto;

namespace SG.DataRetention.Chat.Exporting
{
    public interface IChatMessageListExcelExporter
    {
        FileDto ExportToFile(List<ChatMessageExportDto> messages);
    }
}
