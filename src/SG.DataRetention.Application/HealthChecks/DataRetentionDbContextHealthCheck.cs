﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using SG.DataRetention.EntityFrameworkCore;

namespace SG.DataRetention.HealthChecks
{
    public class DataRetentionDbContextHealthCheck : IHealthCheck
    {
        private readonly DatabaseCheckHelper _checkHelper;

        public DataRetentionDbContextHealthCheck(DatabaseCheckHelper checkHelper)
        {
            _checkHelper = checkHelper;
        }

        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            if (_checkHelper.Exist("db"))
            {
                return Task.FromResult(HealthCheckResult.Healthy("DataRetentionDbContext connected to database."));
            }

            return Task.FromResult(HealthCheckResult.Unhealthy("DataRetentionDbContext could not connect to database"));
        }
    }
}
