﻿using Abp.AspNetCore.Mvc.Views;

namespace SG.DataRetention.Web.Views
{
    public abstract class DataRetentionRazorPage<TModel> : AbpRazorPage<TModel>
    {
        protected DataRetentionRazorPage()
        {
            LocalizationSourceName = DataRetentionConsts.LocalizationSourceName;
        }
    }
}
