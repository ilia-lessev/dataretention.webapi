﻿using Abp.Auditing;
using Microsoft.AspNetCore.Mvc;

namespace SG.DataRetention.Web.Controllers
{
    public class HomeController : DataRetentionControllerBase
    {
        [DisableAuditing]
        public IActionResult Index()
        {
            return RedirectToAction("Index", "Ui");
        }
    }
}
