﻿using Microsoft.AspNetCore.Antiforgery;

namespace SG.DataRetention.Web.Controllers
{
    public class AntiForgeryController : DataRetentionControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
