﻿using Abp.AspNetCore.Mvc.Authorization;
using SG.DataRetention.Storage;

namespace SG.DataRetention.Web.Controllers
{
    [AbpMvcAuthorize]
    public class ProfileController : ProfileControllerBase
    {
        public ProfileController(ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
        }
    }
}