﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace SG.DataRetention
{
    public class DataRetentionCoreSharedModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DataRetentionCoreSharedModule).GetAssembly());
        }
    }
}