﻿namespace SG.DataRetention
{
    public class DataRetentionConsts
    {
        public const string LocalizationSourceName = "DataRetention";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;

        public const bool AllowTenantsToChangeEmailSettings = false;

        public const string Currency = "USD";

        public const string CurrencySign = "$";

        public const string AbpApiClientUserAgent = "AbpApiClient";

        public const string MegaraConnectionStringName = "MegaraDb";
    }
}