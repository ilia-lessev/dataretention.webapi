﻿namespace SG.DataRetention.MultiTenancy.Payments
{
    public enum SubscriptionPaymentGatewayType
    {
        Paypal = 1,
        Stripe = 2
    }
}
