namespace SG.DataRetention.Friendships
{
    public enum FriendshipState
    {
        Accepted = 1,
        Blocked = 2
    }
}