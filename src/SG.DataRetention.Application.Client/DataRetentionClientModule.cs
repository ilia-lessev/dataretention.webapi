﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace SG.DataRetention
{
    public class DataRetentionClientModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DataRetentionClientModule).GetAssembly());
        }
    }
}
