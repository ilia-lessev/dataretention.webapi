using System.Threading.Tasks;
using Abp.Application.Services;
using SG.DataRetention.Editions.Dto;
using SG.DataRetention.MultiTenancy.Dto;

namespace SG.DataRetention.MultiTenancy
{
    public interface ITenantRegistrationAppService: IApplicationService
    {
        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}