﻿namespace SG.DataRetention.MultiTenancy.Payments.Stripe.Dto
{
    public class StripeUpdateSubscriptionInput
    {
        public long PaymentId { get; set; }
    }
}