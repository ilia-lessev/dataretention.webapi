﻿namespace SG.DataRetention.MultiTenancy.Payments.Stripe.Dto
{
    public class StripeConfigurationDto
    {
        public string PublishableKey { get; set; }
    }
}
