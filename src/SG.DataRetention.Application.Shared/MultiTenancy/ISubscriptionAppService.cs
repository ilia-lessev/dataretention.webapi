﻿using System.Threading.Tasks;
using Abp.Application.Services;

namespace SG.DataRetention.MultiTenancy
{
    public interface ISubscriptionAppService : IApplicationService
    {
        Task DisableRecurringPayments();

        Task EnableRecurringPayments();
    }
}
