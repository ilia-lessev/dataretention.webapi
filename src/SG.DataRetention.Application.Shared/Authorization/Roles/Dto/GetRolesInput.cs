﻿using System.Collections.Generic;

namespace SG.DataRetention.Authorization.Roles.Dto
{
    public class GetRolesInput
    {
        public List<string> Permissions { get; set; }
    }
}
