﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SG.DataRetention.Authorization.Permissions.Dto;

namespace SG.DataRetention.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
