﻿using System.ComponentModel.DataAnnotations;

namespace SG.DataRetention.Authorization.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}
