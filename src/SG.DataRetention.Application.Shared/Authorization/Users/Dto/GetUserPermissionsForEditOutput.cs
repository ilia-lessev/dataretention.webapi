﻿using System.Collections.Generic;
using SG.DataRetention.Authorization.Permissions.Dto;

namespace SG.DataRetention.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}