﻿using System.ComponentModel.DataAnnotations;

namespace SG.DataRetention.Authorization.Accounts.Dto
{
    public class SendEmailActivationLinkInput
    {
        [Required]
        public string EmailAddress { get; set; }
    }
}