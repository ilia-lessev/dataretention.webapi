﻿using System;
using Abp.Application.Services.Dto;

namespace SG.DataRetention.Authorization.Accounts.Dto
{
    public class PasswordHistoryDto : FullAuditedEntityDto
    {
        public int UserId { get; set; }

        public string Password { get; set; }

        public DateTime DateChanged { get; set; }
    }
}
