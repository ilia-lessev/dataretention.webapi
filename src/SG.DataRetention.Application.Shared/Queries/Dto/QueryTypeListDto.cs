﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SG.DataRetention.Queries.Dto
{
    public class QueryTypeListDto : Entity
    {
        public int TypeId { get; set; }
        public string TypeName { get; set; }
    }
}
