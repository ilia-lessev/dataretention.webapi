﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SG.DataRetention.Queries.Dto
{
    public class DepartmentListDto : Entity {      
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
    }
}
