﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace SG.DataRetention.Queries.Dto
{
    public class QueryListDto : FullAuditedEntity
    {
        public int QueryId { get; set; }

        public string QueryReference { get; set; }

        public string AssignedTo { get; set; }

        public string Department { get; set; }

        public string QueryType { get; set; }

        public string QueryDescription { get; set; }

        public string QueryStatus { get; set; }
        public string SLA { get; set; }
        public string DeleteReason { get; set; }
    }
}
