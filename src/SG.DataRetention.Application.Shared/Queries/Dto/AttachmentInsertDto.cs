﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SG.DataRetention.Queries.Dto
{
    public class AttachmentInsertDto : Entity
    {
        public int AttachmentId { get; set; }
        public string AttachmentName { get; set; }  
        public int QueryId { get; set; }
        public QueryCreateDto Query { get; set; }
    }
}
