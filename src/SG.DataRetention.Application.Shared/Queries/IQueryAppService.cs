﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SG.DataRetention.Queries.Dto;
using System.Threading.Tasks;

namespace SG.DataRetention.Queries
{
    public interface IQueryAppService : IApplicationService
    {
        ListResultDto<QueryListDto> GetQueries();
        ListResultDto<DepartmentListDto> GetDepartments();
        Task CreateQuery(QueryCreateDto input);
        ListResultDto<QueryTypeListDto> GetQueryTypes(string departmentSelected);
        Task CloseQuery(int queryId, string reasonDetails);
    }
}
