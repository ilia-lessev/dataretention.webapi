﻿using System;

namespace SG.DataRetention.DemoUiComponents.Dto
{
    public class UploadFileOutput
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }
    }
}
