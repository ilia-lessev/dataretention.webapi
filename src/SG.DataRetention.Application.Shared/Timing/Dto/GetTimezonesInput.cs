﻿using Abp.Configuration;

namespace SG.DataRetention.Timing.Dto
{
    public class GetTimezonesInput
    {
        public SettingScopes DefaultTimezoneScope { get; set; }
    }
}
