﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.MultiTenancy;

namespace SG.DataRetention.DataQueries.Common.Dto
{
    public class DataPagingInput
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}