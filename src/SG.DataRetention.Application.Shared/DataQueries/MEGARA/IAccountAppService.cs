﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SG.DataRetention.Authorization.Accounts.Dto;
using SG.DataRetention.DataQueries.Common.Dto;
using SG.DataRetention.DataQueries.Megara.Dto;

namespace SG.DataRetention.DataQueries.Megara
{
    
    public interface IMegaraAppService : IApplicationService
    {
        Task<long> CountClientInstructionsBonds(ClientInstructionsBondsParams input);
        Task<ListResultDto<ClientInstructionsBondsOutput>> ClientInstructionsBonds(ClientInstructionsBondsInput input);
        Task<long> CountClientInstructionsEquity(ClientInstructionsEquityParams input);
        Task<ListResultDto<ClientInstructionsEquityOutput>> ClientInstructionsEquity(ClientInstructionsEquityInput input);
        Task<long> CountClientInstructionsMoneyMarket(ClientInstructionsMoneyMarketParams input);
        Task<ListResultDto<ClientInstructionsMoneyMarketOutput>> ClientInstructionsMoneyMarket(ClientInstructionsMoneyMarketInput input);
        Task<long> CountClientPosition(ClientPositionParams input);
        Task<ListResultDto<ClientPositionOutput>> ClientPosition(ClientInstructionsClientPositionInput input); 
        Task<long> CountDematRematOperations(DematRematOperationsParams input);
        Task<ListResultDto<DematRematOperationsOutput>> DematRematOperations(DematRematOperationsInput input);
        
    }
}
