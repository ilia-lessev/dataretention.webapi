﻿using Newtonsoft.Json;
using System;


namespace SG.DataRetention.DataQueries.Megara.Dto
{
    public class ClientInstructionsMoneyMarketParams
    {
        [JsonProperty(PropertyName = "Main Reference")]
        public string MainReference { get; set; }

        [JsonProperty(PropertyName = "Client Ref")]
        public string ClientRef { get; set; }

        [JsonProperty(PropertyName = "App Ref")]
        public string AppRef { get; set; }

        [JsonProperty(PropertyName = "Client")]
        public string Client { get; set; }

        [JsonProperty(PropertyName = "Sec Acc")]
        public string SecAcc { get; set; }

        [JsonProperty(PropertyName = "Trans Type")]
        public string TransType { get; set; }

        [JsonProperty(PropertyName = "FI")]
        public string FI { get; set; }

        [JsonProperty(PropertyName = "Quantity")]
        public int? Quantity { get; set; }

        [JsonProperty(PropertyName = "Counterpart")] 
        public string Counterpart { get; set; }

        [JsonProperty(PropertyName = "Client Inst Status")]
        public string ClientInstStatus { get; set; }

        [JsonProperty(PropertyName = "Nostro Sec Acc")]
        public string NostroSecAcc { get; set; }

        [JsonProperty(PropertyName = "Trade Date")]
        public DateTime? TradeDate { get; set; }

        [JsonProperty(PropertyName = "Trade Date End")]
        public DateTime? TradeDateEnd { get; set; }

        [JsonProperty(PropertyName = "Sett Date")]
        public DateTime? SettDate { get; set; }

        [JsonProperty(PropertyName = "Sett Date End")]
        public DateTime? SettDateEnd { get; set; }

        [JsonProperty(PropertyName = "Creation Date")]
        public DateTime? CreationDate { get; set; }

        [JsonProperty(PropertyName = "Creation Date End")]
        public DateTime? CreationDateEnd { get; set; }

        [JsonProperty(PropertyName = "Update Date")]
        public DateTime? UpdateDate { get; set; }

        [JsonProperty(PropertyName = "Update Date End")]
        public DateTime? UpdateDateEnd { get; set; }

        [JsonProperty(PropertyName = "Creator User ID")]
        public string CreatorUserID { get; set; }

        [JsonProperty(PropertyName = "Update User ID")]
        public string UpdateUserID { get; set; }

        [JsonProperty(PropertyName = "Custodian")]
        public string Custodian { get; set; }

    }
}