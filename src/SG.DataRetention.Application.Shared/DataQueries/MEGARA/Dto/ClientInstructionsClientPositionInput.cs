﻿using SG.DataRetention.DataQueries.Common.Dto;
using System;


namespace SG.DataRetention.DataQueries.Megara.Dto
{
    public class ClientInstructionsClientPositionInput
    {
        public DataPagingInput Paging { get; set; }
        public ClientPositionParams QueryParams { get; set; }

    }
}