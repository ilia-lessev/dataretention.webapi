﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.MultiTenancy;
using Newtonsoft.Json;

namespace SG.DataRetention.DataQueries.Megara.Dto
{
    public class ClientInstructionsBondsOutput
    {
        [JsonProperty(PropertyName = "Order ID")]
        public string OrderID { get; set; }

        [JsonProperty(PropertyName = "Sec Acc")]
        public string SecAcc { get; set; }

        [JsonProperty(PropertyName = "Trans Type")]
        public string TransType { get; set; }

        [JsonProperty(PropertyName = "Quantity")]
        public int? Quantity { get; set; }

        [JsonProperty(PropertyName = "FI")]
        public string FI { get; set; }

        [JsonProperty(PropertyName = "Sett Amount")]
        public decimal SettAmount { get; set; }

        [JsonProperty(PropertyName = "Trade Date")]
        public DateTime? TradeDate { get; set; }

        [JsonProperty(PropertyName = "Sett Date")]
        public DateTime? SettDate { get; set; }

        [JsonProperty(PropertyName = "Cpty")]
        public string Cpty { get; set; }

        [JsonProperty(PropertyName = "Buyer Account")]
        public string BuyerAccount { get; set; }

        [JsonProperty(PropertyName = "Seller Account")]
        public string SellerAccount { get; set; }

        [JsonProperty(PropertyName = "Client Ref")]
        public string ClientRef { get; set; }

        [JsonProperty(PropertyName = "Taxable")]
        public string Taxable { get; set; }

        [JsonProperty(PropertyName = "Client Inst Status")]
        public string ClientInstStatus { get; set; }

        [JsonProperty(PropertyName = "Mkt Inst Status")]
        public string MktInstStatus { get; set; }

        [JsonProperty(PropertyName = "AFFI / NAFI")]
        public string AFFI_NAFI { get; set; }

        [JsonProperty(PropertyName = "CP BTB")]
        public string CP_BTB { get; set; }

        [JsonProperty(PropertyName = "CP REF")]
        public string CP_REF { get; set; }

        [JsonProperty(PropertyName = "GROUP REF")]
        public string GROUP_REF { get; set; }

        [JsonProperty(PropertyName = "Link Type")]
        public string LinkType { get; set; }

        [JsonProperty(PropertyName = "Linked Ref")]
        public string LinkedRef { get; set; }

        [JsonProperty(PropertyName = "DependencyLink")]
        public string DependencyLink { get; set; }

        [JsonProperty(PropertyName = "Payment Reference")]
        public string PaymentReference { get; set; }

        [JsonProperty(PropertyName = "Last Notification To Client")]
        public string LastNotificationToClient { get; set; }

        [JsonProperty(PropertyName = "Last Notification To JSE")]
        public string LastNotificationToJSE { get; set; }

        [JsonProperty(PropertyName = "Sub Status")]
        public string SubStatus { get; set; }

        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "ISIN")]
        public string ISIN { get; set; }

        [JsonProperty(PropertyName = "Off Ex")]
        public string OffEx { get; set; }

        [JsonProperty(PropertyName = "Sett Diff")]
        public string SettDiff { get; set; }

        [JsonProperty(PropertyName = "Being Cancel")]
        public string BeingCancel { get; set; }

        [JsonProperty(PropertyName = "Cancel Manually")]
        public string CancelManually { get; set; }

        [JsonProperty(PropertyName = "Committed")]
        public string Committed { get; set; }

        [JsonProperty(PropertyName = "Trans Type Description")]
        public string TransTypeDescription { get; set; }

        [JsonProperty(PropertyName = "Notification Type")]
        public string NotificationType { get; set; }

        [JsonProperty(PropertyName = "Buyer Ref")]
        public string BuyerRef { get; set; }

        [JsonProperty(PropertyName = "Seller Ref")]
        public string SellerRef { get; set; }

        [JsonProperty(PropertyName = "Nostro Sec Acc")]
        public string NostroSecAcc { get; set; }

        [JsonProperty(PropertyName = "App Ref")]
        public string AppRef { get; set; }

        [JsonProperty(PropertyName = "Int Match Status")]
        public string IntMatchStatus { get; set; }

        [JsonProperty(PropertyName = "Processed")]
        public string Processed { get; set; }

        [JsonProperty(PropertyName = "Manual Input")]
        public string ManualInput { get; set; }        

        [JsonProperty(PropertyName = "Creation Date")]
        public DateTime? CreationDate { get; set; }

        [JsonProperty(PropertyName = "Update Date")]
        public DateTime? UpdateDate { get; set; }

        [JsonProperty(PropertyName = "Creator User ID")]
        public string CreatorUserID { get; set; }

        [JsonProperty(PropertyName = "Update User ID")]
        public string UpdateUserID { get; set; }
    }
}