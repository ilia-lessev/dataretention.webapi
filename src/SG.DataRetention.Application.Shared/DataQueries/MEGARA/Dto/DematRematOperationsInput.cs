﻿using SG.DataRetention.DataQueries.Common.Dto;
using System;


namespace SG.DataRetention.DataQueries.Megara.Dto
{
    public class DematRematOperationsInput
    {
        public DataPagingInput Paging { get; set; }
        public DematRematOperationsParams QueryParams { get; set; }

    }
}