﻿using SG.DataRetention.DataQueries.Common.Dto;
using System;


namespace SG.DataRetention.DataQueries.Megara.Dto
{
    public class ClientInstructionsEquityInput
    {
        public DataPagingInput Paging { get; set; }
        public ClientInstructionsEquityParams QueryParams { get; set; }

    }
}