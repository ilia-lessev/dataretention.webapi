﻿using SG.DataRetention.DataQueries.Common.Dto;
using System;


namespace SG.DataRetention.DataQueries.Megara.Dto
{
    public class ClientInstructionsMoneyMarketInput
    {
        public DataPagingInput Paging { get; set; }
        public ClientInstructionsMoneyMarketParams QueryParams { get; set; }

    }
}