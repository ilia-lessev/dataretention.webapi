﻿using SG.DataRetention.DataQueries.Common.Dto;
using System;


namespace SG.DataRetention.DataQueries.Megara.Dto
{
    public class ClientInstructionsBondsInput
    {
        public DataPagingInput Paging { get; set; }
        public ClientInstructionsBondsParams QueryParams { get; set; }

    }
}