﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.MultiTenancy;
using Newtonsoft.Json;

namespace SG.DataRetention.DataQueries.Megara.Dto
{
    public class DematRematOperationsOutput
    {
        [JsonProperty(PropertyName = "Order ID")]
        public string OrderID { get; set; }

        [JsonProperty(PropertyName = "App Ref")]
        public string AppRef { get; set; }

        [JsonProperty(PropertyName = "Client Ref")]
        public string ClientRef { get; set; }

        [JsonProperty(PropertyName = "Sec Acc")]
        public string SecAcc { get; set; }

        [JsonProperty(PropertyName = "Trans Type")]
        public string TransType { get; set; }

        [JsonProperty(PropertyName = "Quantity")]
        public int? Quantity { get; set; }

        [JsonProperty(PropertyName = "FI")]
        public string FI { get; set; }

        [JsonProperty(PropertyName = "Sett Date")]
        public DateTime? SettDate { get; set; }

        [JsonProperty(PropertyName = "Physical Leg Status")]
        public string PhysicalLegStatus { get; set; }

        [JsonProperty(PropertyName = "Electronic Leg Status")]
        public string ElectronicLegStatus { get; set; }

        [JsonProperty(PropertyName = "Last Notif In")]
        public string LastNotifIn { get; set; }

        [JsonProperty(PropertyName = "Cancel Manually")]
        public string CancelManually { get; set; }

        [JsonProperty(PropertyName = "Receiver Name")]
        public string ReceiverName { get; set; }

        [JsonProperty(PropertyName = "Receiver Address")]
        public string ReceiverAddress { get; set; }

        [JsonProperty(PropertyName = "Being Cancel")]
        public string BeingCancel { get; set; }

        [JsonProperty(PropertyName = "Creation Date")]
        public DateTime? CreationDate { get; set; }

        [JsonProperty(PropertyName = "Update Date")]
        public DateTime? UpdateDate { get; set; }

        [JsonProperty(PropertyName = "Creator User ID")]
        public string CreatorUserID { get; set; }

        [JsonProperty(PropertyName = "Update User ID")]
        public string UpdateUserID { get; set; }
    }
}