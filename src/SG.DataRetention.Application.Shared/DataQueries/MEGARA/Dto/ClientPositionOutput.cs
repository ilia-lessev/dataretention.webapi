﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.MultiTenancy;
using Newtonsoft.Json;

namespace SG.DataRetention.DataQueries.Megara.Dto
{
    public class ClientPositionOutput
    {
        [JsonProperty(PropertyName = "Position Date")]
        public DateTime? PositionDate { get; set; }

        [JsonProperty(PropertyName = "Sec Acc")]
        public string SecAcc { get; set; }

        [JsonProperty(PropertyName = "Quantity")]
        public int? Quantity { get; set; }

        [JsonProperty(PropertyName = "Tradable Asset")]
        public string TradableAsset { get; set; }

        [JsonProperty(PropertyName = "Position Depo Id")]
        public string PositionDepoId { get; set; }

        [JsonProperty(PropertyName = "Custodian")]
        public string Custodian { get; set; }

        [JsonProperty(PropertyName = "Depo")]
        public string Depo { get; set; }

        [JsonProperty(PropertyName = "Position Nature")]
        public string PositionNature { get; set; }

        [JsonProperty(PropertyName = "Asset Family")]
        public string AssetFamily { get; set; }

        [JsonProperty(PropertyName = "App Ref")]
        public string AppRef { get; set; }

        [JsonProperty(PropertyName = "Validity Date")]
        public DateTime? ValidityDate { get; set; }

        [JsonProperty(PropertyName = "Quantity Type")]
        public string QuantityType { get; set; }

        [JsonProperty(PropertyName = "Position Identifier")]
        public string PositionIdentifier { get; set; }

        [JsonProperty(PropertyName = "Main Reference")]
        public string MainReference { get; set; }
    }
}