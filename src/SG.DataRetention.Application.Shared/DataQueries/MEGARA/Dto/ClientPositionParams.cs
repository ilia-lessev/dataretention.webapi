﻿using Newtonsoft.Json;
using System;


namespace SG.DataRetention.DataQueries.Megara.Dto
{
    public class ClientPositionParams
    {
        [JsonProperty(PropertyName = "Client")]
        public string Client { get; set; }

        [JsonProperty(PropertyName = "Position Date")]
        public DateTime? PositionDate { get; set; }

        [JsonProperty(PropertyName = "Position Date End")]
        public DateTime? PositionDateEnd { get; set; }

        [JsonIgnore]
        public DateTime? ValidityDate { get; set; }

        [JsonProperty(PropertyName = "Origin")]
        public string Origin { get; set; }

        [JsonProperty(PropertyName = "Counterpart Code")]
        public string CounterpartCode { get; set; }

        [JsonProperty(PropertyName = "Sec Acc")]
        public string SecAcc { get; set; }

        [JsonProperty(PropertyName = "Impacted")]
        public string Impacted { get; set; }

        [JsonProperty(PropertyName = "Tradable Asset")]
        public string TradableAsset { get; set; }

        [JsonProperty(PropertyName = "Depo")]
        public string Depo { get; set; }

        [JsonProperty(PropertyName = "Quantity")]
        public int? Quantity { get; set; }

        [JsonProperty(PropertyName = "Processed")]
        public string Processed { get; set; }

        [JsonProperty(PropertyName = "Position Nature")]
        public string PositionNature { get; set; }

        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "Proxy")]
        public string Proxy { get; set; }

        [JsonProperty(PropertyName = "Asset Nature")]
        public string AssetNature { get; set; }

        [JsonProperty(PropertyName = "Main Reference")]
        public string MainReference { get; set; }

        [JsonProperty(PropertyName = "Position Identifier")]
        public string PositionIdentifier { get; set; }

        [JsonProperty(PropertyName = "Creation Date")]
        public DateTime? CreationDate { get; set; }

        [JsonProperty(PropertyName = "Update Date")]
        public DateTime? UpdateDate { get; set; }

        [JsonProperty(PropertyName = "Creation Date End")]
        public DateTime? CreationDateEnd { get; set; }

        [JsonProperty(PropertyName = "Update Date End")]
        public DateTime? UpdateDateEnd { get; set; }
    }
}