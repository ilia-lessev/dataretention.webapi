﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.MultiTenancy;
using Newtonsoft.Json;

namespace SG.DataRetention.DataQueries.Megara.Dto
{
    public class ClientInstructionsMoneyMarketOutput
    {
        [JsonProperty(PropertyName = "Order ID")]
        public string OrderID { get; set; }

        [JsonProperty(PropertyName = "Sec Acc")]
        public string SecAcc { get; set; }

        [JsonProperty(PropertyName = "Trans Type")]
        public string TransType { get; set; }

        [JsonProperty(PropertyName = "Quantity")]
        public int? Quantity { get; set; }

        [JsonProperty(PropertyName = "FI")]
        public string FI { get; set; }

        [JsonProperty(PropertyName = "Set Amount")]
        public decimal SetAmount { get; set; }

        [JsonProperty(PropertyName = "Trade Date")]
        public DateTime? TradeDate { get; set; }

        [JsonProperty(PropertyName = "Sett Date")]
        public DateTime? SettDate { get; set; }

        [JsonProperty(PropertyName = "Cpty")]
        public string Cpty { get; set; }

        [JsonProperty(PropertyName = "Buyer Ref")]
        public string BuyerRef { get; set; }

        [JsonProperty(PropertyName = "Seller Ref")]
        public string SellerRef { get; set; }

        [JsonProperty(PropertyName = "Taxable")]
        public string Taxable { get; set; }

        [JsonProperty(PropertyName = "Client Ref")]
        public string ClientRef { get; set; }

        [JsonProperty(PropertyName = "Client Inst Status")]
        public string ClientInstStatus { get; set; }

        [JsonProperty(PropertyName = "Mkt Inst Status")]
        public string MktInstStatus { get; set; }

        [JsonProperty(PropertyName = "Sub Status")]
        public string SubStatus { get; set; }

        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "App Ref")]
        public string AppRef { get; set; }

        [JsonProperty(PropertyName = "ISIN")]
        public string ISIN { get; set; }

        [JsonProperty(PropertyName = "Off Ex")]
        public string OffEx { get; set; }

        [JsonProperty(PropertyName = "Notification Type")]
        public string NotificationType { get; set; }

        [JsonProperty(PropertyName = "Sett Diff")]
        public string SettDiff { get; set; }

        [JsonProperty(PropertyName = "Linked Ref")]
        public string LinkedRef { get; set; }

        [JsonProperty(PropertyName = "Being Cancel")]
        public bool BeingCancel { get; set; }

        [JsonProperty(PropertyName = "Cancel Manually")]
        public bool CancelManually { get; set; }

        [JsonProperty(PropertyName = "Int Match Status")]
        public string IntMatchStatus { get; set; }

        [JsonProperty(PropertyName = "Processed")]
        public bool Processed { get; set; }

        [JsonProperty(PropertyName = "Nostro Sec Acc")]
        public string NostroSecAcc { get; set; }

        [JsonProperty(PropertyName = "Creator User ID")]
        public string CreatorUserID { get; set; }

        [JsonProperty(PropertyName = "Update User ID")]
        public string UpdateUserID { get; set; }

        [JsonProperty(PropertyName = "Creation Date")]
        public DateTime? CreationDate { get; set; }

        [JsonProperty(PropertyName = "Update Date")]
        public DateTime? UpdateDate { get; set; }

        [JsonProperty(PropertyName = "Manual Input")]
        public bool ManualInput { get; set; }

        [JsonProperty(PropertyName = "Committed")]
        public bool Committed { get; set; }

        [JsonProperty(PropertyName = "Trans Type Description")]
        public string TransTypeDescription { get; set; }

        [JsonProperty(PropertyName = "Last Notification to Client")]
        public string LastNotificationToClient { get; set; }
    }
}