﻿namespace SG.DataRetention.Tenants.Dashboard.Dto
{
    public class GetProfitShareOutput
    {
        public int[] ProfitShares { get; set; }
    }
}