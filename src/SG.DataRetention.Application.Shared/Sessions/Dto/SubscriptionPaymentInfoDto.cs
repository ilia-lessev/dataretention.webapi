﻿namespace SG.DataRetention.Sessions.Dto
{
    public class SubscriptionPaymentInfoDto
    {
        public decimal Amount { get; set; }
    }
}