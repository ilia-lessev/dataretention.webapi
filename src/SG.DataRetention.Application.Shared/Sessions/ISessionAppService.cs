﻿using System.Threading.Tasks;
using Abp.Application.Services;
using SG.DataRetention.Sessions.Dto;

namespace SG.DataRetention.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();

        Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken();
    }
}
