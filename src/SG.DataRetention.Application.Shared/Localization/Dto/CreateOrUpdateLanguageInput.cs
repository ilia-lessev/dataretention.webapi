﻿using System.ComponentModel.DataAnnotations;

namespace SG.DataRetention.Localization.Dto
{
    public class CreateOrUpdateLanguageInput
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}