﻿using System.Collections.Generic;

namespace SG.DataRetention.Logging.Dto
{
    public class GetLatestWebLogsOutput
    {
        public List<string> LatestWebLogLines { get; set; }
    }
}
