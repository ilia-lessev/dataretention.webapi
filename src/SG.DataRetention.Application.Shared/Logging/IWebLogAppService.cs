﻿using Abp.Application.Services;
using SG.DataRetention.Dto;
using SG.DataRetention.Logging.Dto;

namespace SG.DataRetention.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}
