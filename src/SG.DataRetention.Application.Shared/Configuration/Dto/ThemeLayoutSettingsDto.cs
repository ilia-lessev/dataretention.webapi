namespace SG.DataRetention.Configuration.Dto
{
    public class ThemeLayoutSettingsDto
    {
        public string LayoutType { get; set; }
    }
}