﻿using System.Threading.Tasks;
using Abp.Application.Services;
using SG.DataRetention.Configuration.Host.Dto;

namespace SG.DataRetention.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);

        Task SendTestEmail(SendTestEmailInput input);
    }
}
