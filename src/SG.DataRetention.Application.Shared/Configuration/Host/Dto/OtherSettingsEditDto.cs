﻿namespace SG.DataRetention.Configuration.Host.Dto
{
    public class OtherSettingsEditDto
    {
        public bool IsQuickThemeSelectEnabled { get; set; }
    }
}