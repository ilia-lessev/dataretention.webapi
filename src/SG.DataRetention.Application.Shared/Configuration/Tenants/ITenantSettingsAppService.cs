﻿using System.Threading.Tasks;
using Abp.Application.Services;
using SG.DataRetention.Configuration.Tenants.Dto;

namespace SG.DataRetention.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);

        Task ClearLogo();

        Task ClearCustomCss();
    }
}
