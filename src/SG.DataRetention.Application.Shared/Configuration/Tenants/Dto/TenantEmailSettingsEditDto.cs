﻿using Abp.Auditing;
using SG.DataRetention.Configuration.Dto;

namespace SG.DataRetention.Configuration.Tenants.Dto
{
    public class TenantEmailSettingsEditDto : EmailSettingsEditDto
    {
        public bool UseHostDefaultEmailSettings { get; set; }
    }
}