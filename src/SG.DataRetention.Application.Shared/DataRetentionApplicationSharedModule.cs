﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace SG.DataRetention
{
    [DependsOn(typeof(DataRetentionCoreSharedModule))]
    public class DataRetentionApplicationSharedModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DataRetentionApplicationSharedModule).GetAssembly());
        }
    }
}