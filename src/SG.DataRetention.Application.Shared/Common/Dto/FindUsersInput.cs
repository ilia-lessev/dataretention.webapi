﻿using SG.DataRetention.Dto;

namespace SG.DataRetention.Common.Dto
{
    public class FindUsersInput : PagedAndFilteredInputDto
    {
        public int? TenantId { get; set; }
    }
}