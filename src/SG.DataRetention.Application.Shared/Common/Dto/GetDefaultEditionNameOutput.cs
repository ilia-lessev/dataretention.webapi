﻿namespace SG.DataRetention.Common.Dto
{
    public class GetDefaultEditionNameOutput
    {
        public string Name { get; set; }
    }
}
