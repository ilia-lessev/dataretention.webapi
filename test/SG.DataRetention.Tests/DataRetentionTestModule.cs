﻿using Abp.Modules;
using SG.DataRetention.Test.Base;

namespace SG.DataRetention.Tests
{
    [DependsOn(typeof(DataRetentionTestBaseModule))]
    public class DataRetentionTestModule : AbpModule
    {
       
    }
}
