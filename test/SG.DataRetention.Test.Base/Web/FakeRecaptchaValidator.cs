﻿using System.Threading.Tasks;
using SG.DataRetention.Security.Recaptcha;

namespace SG.DataRetention.Test.Base.Web
{
    public class FakeRecaptchaValidator : IRecaptchaValidator
    {
        public Task ValidateAsync(string captchaResponse)
        {
            return Task.CompletedTask;
        }
    }
}
