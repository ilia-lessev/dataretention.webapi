﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Castle.Windsor.MsDependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using SG.DataRetention.Configure;
using SG.DataRetention.Startup;
using SG.DataRetention.Test.Base;

namespace SG.DataRetention.GraphQL.Tests
{
    [DependsOn(
        typeof(DataRetentionGraphQLModule),
        typeof(DataRetentionTestBaseModule))]
    public class DataRetentionGraphQLTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            IServiceCollection services = new ServiceCollection();
            
            services.AddAndConfigureGraphQL();

            WindsorRegistrationHelper.CreateServiceProvider(IocManager.IocContainer, services);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DataRetentionGraphQLTestModule).GetAssembly());
        }
    }
}